import 'package:base_flutter/app.dart';
import 'package:base_flutter/app/config/env.dart';
import 'package:base_flutter/di/di.dart';
import 'package:flutter/material.dart';

void main() {
  initDI(ENVType.beta);
  runApp(const MyApp());
}
