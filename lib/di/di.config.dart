// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:base_flutter/app/config/exception/exception.dart' as _i12;
import 'package:base_flutter/app/config/exception/exception_handler_impl.dart'
    as _i13;
import 'package:base_flutter/app/config/network/rest_client/app_rest_client.dart'
    as _i11;
import 'package:base_flutter/app/feature/login/bloc/login_cubit.dart' as _i3;
import 'package:base_flutter/app/repository/auth/auth_repository.dart' as _i8;
import 'package:base_flutter/app/route/route.dart' as _i4;
import 'package:base_flutter/app/use_case/auth/login_use_case.dart' as _i14;
import 'package:base_flutter/app/util/call_util.dart' as _i5;
import 'package:base_flutter/app/util/number_util.dart' as _i6;
import 'package:base_flutter/app/util/widget_util.dart' as _i7;
import 'package:flutter_secure_storage/flutter_secure_storage.dart' as _i9;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;
import 'package:shared_preferences/shared_preferences.dart' as _i10;

extension GetItInjectableX on _i1.GetIt {
// initializes the registration of main-scope dependencies inside of GetIt
  _i1.GetIt init({
    String? environment,
    _i2.EnvironmentFilter? environmentFilter,
  }) {
    final gh = _i2.GetItHelper(
      this,
      environment,
      environmentFilter,
    );
    gh.factory<_i3.LoginCubit>(() => _i3.LoginCubit());
    gh.lazySingleton<_i4.AppRoute>(() => _i4.AppRoute());
    gh.lazySingleton<_i5.CallUtil>(() => _i5.CallUtil());
    gh.lazySingleton<_i6.NumberUtil>(() => _i6.NumberUtil());
    gh.lazySingleton<_i7.WidgetUtil>(() => _i7.WidgetUtil());
    gh.factory<_i8.AuthRepository>(() => _i8.AuthRepositoryImpl(
          gh<_i9.FlutterSecureStorage>(),
          gh<_i10.SharedPreferences>(),
          gh<_i11.AppRestClient>(),
        ));
    gh.factory<_i12.BaseExceptionHandler>(() => _i13.CommonExceptionHandler());
    gh.factory<_i14.LoginUseCase>(
        () => _i14.LoginUseCase(gh<_i8.AuthRepository>()));
    return this;
  }
}
