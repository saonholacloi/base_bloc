import 'package:base_flutter/design_system/ds_theme.dart';
import 'package:flutter/material.dart';

enum DSPrimaryButtonType { success, warming, custom, disable, plain, clear }

class DSPrimaryButton extends StatelessWidget {
  final String title;
  final GestureTapCallback onTap;
  final bool enable;
  final double width;
  final double height;
  final TextStyle? textStyle;
  final Widget? leftIcon;
  final Widget? rightIcon;
  final Color? bgColor;
  final bool isShadow;
  final DSPrimaryButtonType type;
  final double borderRadius;

  const DSPrimaryButton({
    required this.title,
    required this.onTap,
    this.enable = true,
    this.width = double.infinity,
    this.height = 50,
    this.textStyle,
    this.leftIcon,
    this.rightIcon,
    this.bgColor,
    this.borderRadius = 25,
    this.type = DSPrimaryButtonType.success,
    this.isShadow = true,
    Key? key,
  }) : super(key: key);

  Color _resolveBackgroundColor(BuildContext context) {
    if (type == DSPrimaryButtonType.clear) {
      return DSTheme.of(context).color.mainBackgroundColor;
    }
    if (enable) {
      switch (type) {
        case DSPrimaryButtonType.success:
          return DSTheme.of(context).color.primaryColor;
        case DSPrimaryButtonType.warming:
          return DSTheme.of(context).color.warning;
        case DSPrimaryButtonType.custom:
          return DSTheme.of(context).color.secondaryColor;
        case DSPrimaryButtonType.disable:
          return DSTheme.of(context).color.primaryColor;
        case DSPrimaryButtonType.plain:
          return Colors.white;

        default:
          return DSTheme.of(context).color.primaryColor;
      }
    }

    return DSTheme.of(context).color.grey50;
  }

  Color _resolveTitleTextColor(BuildContext context) {
    if (type == DSPrimaryButtonType.custom) {
      return Colors.white;
    }
    if (type == DSPrimaryButtonType.disable) {
      return DSTheme.of(context).color.grey300;
    }
    if (type == DSPrimaryButtonType.plain) {
      return DSTheme.of(context).color.link;
    }
    if (enable) {
      return Colors.white;
    }

    return DSTheme.of(context).color.grey200;
  }

  double _resolveTitleRadius() {
    switch (type) {
      case DSPrimaryButtonType.clear:
        return 0;
      default:
        return borderRadius;
    }
  }

  double _resolveButtonHeight() {
    switch (type) {
      case DSPrimaryButtonType.clear:
        return 30;
      default:
        return height;
    }
  }

  @override
  Widget build(BuildContext context) => GestureDetector(
        onTap: enable ? onTap : null,
        child: Container(
          decoration: BoxDecoration(
            color: bgColor ?? _resolveBackgroundColor(context),
            boxShadow: isShadow
                ? [
                    DSTheme.of(context).color.boxShadow,
                  ]
                : [],
            borderRadius:
                BorderRadius.all(Radius.circular(_resolveTitleRadius())),
          ),
          width: width,
          height: _resolveButtonHeight(),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              leftIcon ?? const SizedBox.shrink(),
              Text(
                title.toUpperCase(),
                textAlign: TextAlign.center,
                style: textStyle ??
                    DSTheme.of(context)
                        .style
                        .tsQuick16w700
                        .copyWith(color: _resolveTitleTextColor(context)),
              ),
              rightIcon ?? const SizedBox.shrink(),
            ],
          ),
        ),
      );
}
