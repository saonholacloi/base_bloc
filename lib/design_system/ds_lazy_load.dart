import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

enum DSLazyLoadScrollDirection {
  up,
  down,
  none;

  bool isUp() => this == DSLazyLoadScrollDirection.up;
  bool isDown() => this == DSLazyLoadScrollDirection.down;
}

typedef OnLazyLoad = Function(DSLazyLoadScrollDirection direction);

class DSLazyLoad extends StatefulWidget {
  final ScrollView scrollView;
  final OnLazyLoad onLazyLoad;
  const DSLazyLoad(
      {required this.scrollView, required this.onLazyLoad, Key? key})
      : super(key: key);

  @override
  State<DSLazyLoad> createState() => _DSLazyLoadState();
}

class _DSLazyLoadState extends State<DSLazyLoad> {
  double _lastPixelScroll = 0;
  final BehaviorSubject<DSLazyLoadScrollDirection> _behaviorSubject =
      BehaviorSubject();

  @override
  void initState() {
    super.initState();
    _behaviorSubject
        .throttleTime(const Duration(milliseconds: 600))
        .listen((event) {
      widget.onLazyLoad(event);
    });
  }

  @override
  Widget build(BuildContext context) =>
      NotificationListener<ScrollNotification>(
        onNotification: ((ScrollNotification notification) {
          if (notification.metrics.axisDirection == AxisDirection.left ||
              notification.metrics.axisDirection == AxisDirection.right) {
            return false;
          }
          DSLazyLoadScrollDirection direction = DSLazyLoadScrollDirection.none;
          if (notification.metrics.pixels > _lastPixelScroll) {
            direction = DSLazyLoadScrollDirection.up;
          } else if (notification.metrics.pixels < _lastPixelScroll) {
            direction = DSLazyLoadScrollDirection.down;
          }
          _lastPixelScroll = notification.metrics.pixels;

          if (notification.metrics.maxScrollExtent > 0 &&
              notification.metrics.pixels >=
                  (notification.metrics.maxScrollExtent - 100)) {
            _behaviorSubject.add(direction);
          }
          return false;
        }),
        child: widget.scrollView,
      );
}
