import 'package:base_flutter/design_system/ds_theme.dart';
import 'package:base_flutter/design_system/ds_window_area_fitted_box.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class DSScaffold extends StatelessWidget {
  final Widget child;
  final Color? backgroundColor;
  final VoidCallback? onTap;
  final Widget? bottomNavigationBar;
  final SystemUiOverlayStyle systemUiOverlayStyle;
  final EdgeInsets padding;
  final Color? statusBarBackgroundColor;
  final bool safeAreaTop;
  final bool safeAreaLeft;
  final bool safeAreaRight;
  final bool safeAreaBottom;
  final PreferredSizeWidget? appbar;
  final bool isDisableFitTop;
  final Widget? floatingActionButton;

  const DSScaffold(
      {required this.child,
      this.floatingActionButton,
      this.backgroundColor,
      this.onTap,
      this.bottomNavigationBar,
      this.systemUiOverlayStyle = SystemUiOverlayStyle.light,
      this.padding = EdgeInsets.zero,
      this.statusBarBackgroundColor,
      this.safeAreaTop = false,
      this.safeAreaLeft = true,
      this.safeAreaRight = true,
      this.safeAreaBottom = false,
      this.isDisableFitTop = false,
      this.appbar,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) => AnnotatedRegion(
        value: systemUiOverlayStyle,
        child: Scaffold(
          appBar: appbar,
          backgroundColor:
              backgroundColor ?? DSTheme.of(context).color.backgroundColor,
          body: Column(
            children: [
              if (!isDisableFitTop) ...[
                DSWindowAreaFittedBox(
                  fitTop: true,
                  color: statusBarBackgroundColor ??
                      DSTheme.of(context).color.mainBlackColor,
                )
              ],
              Expanded(
                child: GestureDetector(
                    onTap: () {
                      onTap?.call();
                    },
                    child: SafeArea(
                        top: safeAreaTop,
                        left: safeAreaLeft,
                        right: safeAreaRight,
                        bottom: safeAreaBottom,
                        child: Container(
                            padding: padding,
                            color: Colors.transparent,
                            child: child))),
              ),
            ],
          ),
          floatingActionButton: floatingActionButton,
          bottomNavigationBar: bottomNavigationBar,
        ),
      );
}

// return DSScaffold(
// bottomNavigationBar: Padding(
//   padding:
//       const EdgeInsets.symmetric(horizontal: 40, vertical: 20)
//           .copyWith(top: 0),
//   child: DSPrimaryButton(
//       title: S.current.update.toUpperCase(),
//       enable: true,
//       type: DSPrimaryButtonType.success,
//       onTap: () {
//         _cubit.submit();
//         widgetUtil.closeGlobalKeyboard();
//       }),
// ),
// child: Column(
//   children: [
//     DSAppBarDart(
//       title: 'Cài đặt dịch vụ'.hardcode,
//       showChild: true,
//       child: Assets.icons.profile.settings.svg(
//         width: 22,
//         height: 22,
//       ),
//     ),
//     Expanded(
//       child: Padding(
//         padding: const EdgeInsets.all(20),
//         child: ListView(
//           padding: EdgeInsets.zero,
//           children: [
//             for (final s in state.listMainService)
//               CheckBoxWidget(
//                 // value: state.selected.keys.contains(s.id),
//                 value: s.isSelected == 1,
//                 callback: () =>
//                     _cubit.onSelect(serviceBooking: s),
//                 name: s.name ?? '',
//               ),
//           ],
//         ),
//       ),
//     )
//   ],
// ),
// );
