import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DSLoadingIndicator extends StatelessWidget {
  const DSLoadingIndicator({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Container(
        width: double.infinity,
        height: double.infinity,
        color: Colors.black.withOpacity(0),
        child: Center(
          child: Container(
            decoration: BoxDecoration(
              color: Colors.black.withOpacity(0.6),
              borderRadius: const BorderRadius.all(
                Radius.circular(10),
              ),
            ),
            child: const SizedBox(
              width: 120,
              height: 120,
              child: CupertinoActivityIndicator(
                radius: 25,
                color: Colors.white,
              ),
            ),
          ),
        ),
      );
}
