import 'dart:async';

class DSBottomNavigationBarHelper {
  DSBottomNavigationBarHelper._();
  static final DSBottomNavigationBarHelper _instance =
      DSBottomNavigationBarHelper._();
  factory DSBottomNavigationBarHelper() => _instance;

  int selectedIndex = 0;
  final _tabStreamController = StreamController<int>();
  Stream<int>? _stream;
  Stream<int> get stream {
    _stream ??= _tabStreamController.stream.asBroadcastStream();
    return _stream!;
  }

  void jumpToTab(int index) {
    selectedIndex = index;
    _tabStreamController.add(index);
  }
}
