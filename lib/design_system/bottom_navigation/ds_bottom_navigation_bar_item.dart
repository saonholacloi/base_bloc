import 'package:flutter/material.dart';

class DSBottomNavigationBarItem {
  const DSBottomNavigationBarItem(
      {required this.label,
      required this.activeWidget,
      required this.inactiveWidget});

  final String label;
  final Widget activeWidget;
  final Widget inactiveWidget;
}
