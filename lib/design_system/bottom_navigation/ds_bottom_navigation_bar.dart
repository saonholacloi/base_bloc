import 'dart:io';
import 'package:base_flutter/design_system/bottom_navigation/ds_bottom_navigation_bar_helper.dart';
import 'package:base_flutter/design_system/bottom_navigation/ds_bottom_navigation_bar_item.dart';
import 'package:base_flutter/design_system/ds_theme.dart';
import 'package:flutter/material.dart';

class DSBottomNavigationBar extends StatefulWidget {
  const DSBottomNavigationBar(
      {required this.dsBottomNavigationHelper,
      required this.onTap,
      required this.items,
      this.color,
      Key? key})
      : super(key: key);

  final List<DSBottomNavigationBarItem> items;
  final DSBottomNavigationBarHelper dsBottomNavigationHelper;
  final Function(int) onTap;
  final Color? color;

  @override
  State<DSBottomNavigationBar> createState() => _DSBottomNavigationBarState();
}

class _DSBottomNavigationBarState extends State<DSBottomNavigationBar> {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<int>(
        stream: widget.dsBottomNavigationHelper.stream,
        builder: (context, snapshot) {
          return Theme(
            data: Platform.isIOS
                ? Theme.of(context).copyWith(
                    splashColor:
                        DSTheme.of(context).color.skeletonHighlightColor,
                    highlightColor:
                        DSTheme.of(context).color.skeletonHighlightColor,
                  )
                : Theme.of(context),
            child: BottomNavigationBar(
                elevation: 0,
                backgroundColor:
                    DSTheme.of(context).color.skeletonHighlightColor,
                showUnselectedLabels: true,
                unselectedFontSize: 12,
                type: BottomNavigationBarType.fixed,
                selectedItemColor:
                    widget.color ?? DSTheme.of(context).color.red9B111E,
                unselectedItemColor: DSTheme.of(context).color.grey7F7F7F,
                selectedLabelStyle: DSTheme.of(context)
                    .style
                    .tsQuick14w700
                    .copyWith(color: DSTheme.of(context).color.red9B111E),
                unselectedLabelStyle: DSTheme.of(context)
                    .style
                    .tsQuick14w700
                    .copyWith(color: DSTheme.of(context).color.grey7F7F7F),
                items: widget.items
                    .map((e) => BottomNavigationBarItem(
                          label: e.label,
                          activeIcon: e.activeWidget,
                          icon: e.inactiveWidget,
                        ))
                    .toList(),
                currentIndex: snapshot.data ?? 0,
                onTap: widget.onTap),
          );
        });
  }

  @override
  void dispose() {
    widget.dsBottomNavigationHelper.jumpToTab(0);
    super.dispose();
  }
}
