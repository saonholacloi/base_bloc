import 'package:json_annotation/json_annotation.dart';

enum Gender {
  @JsonValue(0)
  male,
  @JsonValue(1)
  female,
  @JsonValue(2)
  total;

  int getRawValue() {
    switch (this) {
      case male:
        return 0;
      case female:
        return 1;
      case total:
        return 2;
    }
  }
}

enum PaymentMethod {
  cash,
  wallet,
}

enum UserType { client, staff, admin }

extension UserTypeX on UserType {
  bool get isStaff => this == UserType.staff;
}

enum TransactionType { expense, receive }

enum StaffSortType {
  nearest,
  newest,
  bestRate,
  foreignLanguage,
}

enum ServiceType { defaultService, vip, interest }

enum BookingStatus {
  statusDefault,
  statusSearchForStaff,
  statusNewAndWaitForAccepting,
  statusWaitingAfterDurationExtended,
  statusCancelled,
  statusReadyAndWaitForComing,
  statusStartedByStaff,
  statusExtendedDuration,
  statusCompleted,
  statusRated,
  statusCancelledDurationExtension,
  statusEdited,
  statusNewFromInterestAndWaitForAccepting
}

enum ResponseOTP {
  verified,
  nonError,
}

enum ChannelType {
  @JsonValue(1)
  privateChat,
  @JsonValue(2)
  bookingChat
}

enum FeedbackType {
  @JsonValue(0)
  common,
  @JsonValue(1)
  message,
  @JsonValue(2)
  imageSharing,
  @JsonValue(3)
  user,
  @JsonValue(4)
  block
}
