abstract class ScreenName {
  static const String root = '/';
  static const String login = '/login';
  static const String onboard = '/onboard';
  static const String register = '/register';
  static const String registerStaff = '/register_staff';

  static const String empty = '/empty';
  static const String debugLog = '/debug_log';
}

abstract class PresentationNotification {
  static const String androidNotificationChannelId = 'tellme_notifications';
  static const String androidNotificationChannelName = 'Tellme notification';
  static const String androidNotificationChannelDescription =
      'Tellme notification';
  static const String icNotification = 'ic_notification';
}
