abstract class NetworkConfig {
  static const int receiveTimeout = 31000;
  static const int connectTimeout = 31000;
  static const sendTimeout = 31000;
}

abstract class StorageConfig {
  static const String keyToken = 'KEY_TOKEN';
  static const String keyRefreshToken = 'KEY_REFRESH_TOKEN';
  static const String keyUserId = 'KEY_USER_ID';
}

abstract class EndPoint {
  static const List<String> listPathNotRequireToken = [];
  // domain
  static const String appDomain = '/app';

  /// Auth
  static const String registerClient = '$appDomain/auth/register-as-client';
}

abstract class ErrorMessage {
  static const invalidResponse = 'Invalid response';
}

abstract class HttpCode {
  static const int unauthorized = 401;
  static const int maintain = 503;
  static const int badGateway = 502;
  static const int system = 500;
}

abstract class BookingSocketMessageType {
  static const String NEW_BOOKING = 'NEW_BOOKING';
  static const String STAFF_ACCEPT = 'STAFF_ACCEPT';
  static const String CLIENT_SELECT = 'CLIENT_SELECT';
  static const String CLIENT_NOT_SELECT = 'CLIENT_NOT_SELECT';
  static const String STAFF_READY = 'STAFF_READY';
  static const String STARTED_BOOKING = 'STARTED_BOOKING';
  static const String CLIENT_CANCEL = 'CLIENT_CANCEL';
  static const String STAFF_CANCEL = 'STAFF_CANCEL';
  static const String MEMBER_LOCATION = 'MEMBER_LOCATION';
  static const String MEMBER_ARRIVED = 'MEMBER_ARRIVED';
  static const String COMPLETED_BOOKING = 'COMPLETED_BOOKING';
  static const String NEW_SCHEDULED_BOOKING = 'NEW_SCHEDULED_BOOKING';
  static const String STAFF_ACCEPT_SCHEDULED = 'STAFF_ACCEPT_SCHEDULED';
  static const String STAFF_CANCEL_SCHEDULED = 'STAFF_CANCEL_SCHEDULED';
  static const String CLIENT_CANCEL_SCHEDULED = 'CLIENT_CANCEL_SCHEDULED';
  static const String NEAR_COMPLETED_BOOKING = 'NEAR_COMPLETED_BOOKING';
  static const String COMPLETED_BOOKING_ONE_STAFF =
      'COMPLETED_BOOKING_ONE_STAFF';
  static const String NEAR_START_BOOKING = 'NEAR_START_BOOKING';
  static const String STARTED_SCHEDULED_BOOKING = 'STARTED_SCHEDULED_BOOKING';
  static const String CLIENT_CANCEL_NO_MEMBER = 'CLIENT_CANCEL_NO_MEMBER';
}

abstract class BadgeConfig {
  static const String notification = 'notification';
}

String keyData = 'data';
