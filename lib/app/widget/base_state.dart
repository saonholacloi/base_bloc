import 'package:base_flutter/app/util/call_util.dart';
import 'package:base_flutter/app/util/widget_util.dart';
import 'package:base_flutter/di/di.dart';
import 'package:flutter/material.dart';

abstract class BaseState<T extends StatefulWidget> extends State<T> {
  WidgetUtil get widgetUtil => di();
  CallUtil get callUtil => di();
  // FCMUtil get fcmUtil => di();

  Route get route => di();
}
