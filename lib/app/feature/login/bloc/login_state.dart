import 'package:base_flutter/app/config/exception/exception.dart';
import 'package:base_flutter/app/feature/login/view_model/login_view_model.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'login_state.freezed.dart';

@freezed
abstract class LoginState with _$LoginState {
  const LoginState._();

  const factory LoginState.loginPrimaryState({
    @Default(LoginViewModel()) LoginViewModel viewModel,
  }) = LoginPrimaryState;

  const factory LoginState.loginLoadingState({
    @Default(LoginViewModel()) LoginViewModel viewModel,
    @Default(true) bool shouldShowLoading,
  }) = LoginLoadingState;

  const factory LoginState.loginChangeFormState({
    @Default(LoginViewModel()) LoginViewModel viewModel,
  }) = LoginChangeFormState;

  const factory LoginState.loginSuccessState({
    @Default(LoginViewModel()) LoginViewModel viewModel,
  }) = LoginSuccessState;
  const factory LoginState.loginPhoneVerified({
    @Default(LoginViewModel()) LoginViewModel viewModel,
  }) = LoginPhoneVerified;
  const factory LoginState.loginPhoneNotVerified({
    @Default(LoginViewModel()) LoginViewModel viewModel,
  }) = LoginPhoneNotVerified;
  const factory LoginState.resetPinCode({
    @Default(LoginViewModel()) LoginViewModel viewModel,
  }) = ResetPinCodeState;
  const factory LoginState.loginCheckPhoneSuccessState({
    @Default(LoginViewModel()) LoginViewModel viewModel,
  }) = LoginCheckPhoneSuccessState;

  const factory LoginState.loginErrorState({
    @Default(LoginViewModel()) LoginViewModel viewModel,
    @Default(BaseException()) BaseException exception,
  }) = LoginErrorState;
}
