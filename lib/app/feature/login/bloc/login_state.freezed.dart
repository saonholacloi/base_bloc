// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'login_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$LoginState {
  LoginViewModel get viewModel => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(LoginViewModel viewModel) loginPrimaryState,
    required TResult Function(LoginViewModel viewModel, bool shouldShowLoading)
        loginLoadingState,
    required TResult Function(LoginViewModel viewModel) loginChangeFormState,
    required TResult Function(LoginViewModel viewModel) loginSuccessState,
    required TResult Function(LoginViewModel viewModel) loginPhoneVerified,
    required TResult Function(LoginViewModel viewModel) loginPhoneNotVerified,
    required TResult Function(LoginViewModel viewModel) resetPinCode,
    required TResult Function(LoginViewModel viewModel)
        loginCheckPhoneSuccessState,
    required TResult Function(LoginViewModel viewModel, BaseException exception)
        loginErrorState,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(LoginViewModel viewModel)? loginPrimaryState,
    TResult? Function(LoginViewModel viewModel, bool shouldShowLoading)?
        loginLoadingState,
    TResult? Function(LoginViewModel viewModel)? loginChangeFormState,
    TResult? Function(LoginViewModel viewModel)? loginSuccessState,
    TResult? Function(LoginViewModel viewModel)? loginPhoneVerified,
    TResult? Function(LoginViewModel viewModel)? loginPhoneNotVerified,
    TResult? Function(LoginViewModel viewModel)? resetPinCode,
    TResult? Function(LoginViewModel viewModel)? loginCheckPhoneSuccessState,
    TResult? Function(LoginViewModel viewModel, BaseException exception)?
        loginErrorState,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(LoginViewModel viewModel)? loginPrimaryState,
    TResult Function(LoginViewModel viewModel, bool shouldShowLoading)?
        loginLoadingState,
    TResult Function(LoginViewModel viewModel)? loginChangeFormState,
    TResult Function(LoginViewModel viewModel)? loginSuccessState,
    TResult Function(LoginViewModel viewModel)? loginPhoneVerified,
    TResult Function(LoginViewModel viewModel)? loginPhoneNotVerified,
    TResult Function(LoginViewModel viewModel)? resetPinCode,
    TResult Function(LoginViewModel viewModel)? loginCheckPhoneSuccessState,
    TResult Function(LoginViewModel viewModel, BaseException exception)?
        loginErrorState,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoginPrimaryState value) loginPrimaryState,
    required TResult Function(LoginLoadingState value) loginLoadingState,
    required TResult Function(LoginChangeFormState value) loginChangeFormState,
    required TResult Function(LoginSuccessState value) loginSuccessState,
    required TResult Function(LoginPhoneVerified value) loginPhoneVerified,
    required TResult Function(LoginPhoneNotVerified value)
        loginPhoneNotVerified,
    required TResult Function(ResetPinCodeState value) resetPinCode,
    required TResult Function(LoginCheckPhoneSuccessState value)
        loginCheckPhoneSuccessState,
    required TResult Function(LoginErrorState value) loginErrorState,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(LoginPrimaryState value)? loginPrimaryState,
    TResult? Function(LoginLoadingState value)? loginLoadingState,
    TResult? Function(LoginChangeFormState value)? loginChangeFormState,
    TResult? Function(LoginSuccessState value)? loginSuccessState,
    TResult? Function(LoginPhoneVerified value)? loginPhoneVerified,
    TResult? Function(LoginPhoneNotVerified value)? loginPhoneNotVerified,
    TResult? Function(ResetPinCodeState value)? resetPinCode,
    TResult? Function(LoginCheckPhoneSuccessState value)?
        loginCheckPhoneSuccessState,
    TResult? Function(LoginErrorState value)? loginErrorState,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoginPrimaryState value)? loginPrimaryState,
    TResult Function(LoginLoadingState value)? loginLoadingState,
    TResult Function(LoginChangeFormState value)? loginChangeFormState,
    TResult Function(LoginSuccessState value)? loginSuccessState,
    TResult Function(LoginPhoneVerified value)? loginPhoneVerified,
    TResult Function(LoginPhoneNotVerified value)? loginPhoneNotVerified,
    TResult Function(ResetPinCodeState value)? resetPinCode,
    TResult Function(LoginCheckPhoneSuccessState value)?
        loginCheckPhoneSuccessState,
    TResult Function(LoginErrorState value)? loginErrorState,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $LoginStateCopyWith<LoginState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LoginStateCopyWith<$Res> {
  factory $LoginStateCopyWith(
          LoginState value, $Res Function(LoginState) then) =
      _$LoginStateCopyWithImpl<$Res, LoginState>;
  @useResult
  $Res call({LoginViewModel viewModel});

  $LoginViewModelCopyWith<$Res> get viewModel;
}

/// @nodoc
class _$LoginStateCopyWithImpl<$Res, $Val extends LoginState>
    implements $LoginStateCopyWith<$Res> {
  _$LoginStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? viewModel = null,
  }) {
    return _then(_value.copyWith(
      viewModel: null == viewModel
          ? _value.viewModel
          : viewModel // ignore: cast_nullable_to_non_nullable
              as LoginViewModel,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $LoginViewModelCopyWith<$Res> get viewModel {
    return $LoginViewModelCopyWith<$Res>(_value.viewModel, (value) {
      return _then(_value.copyWith(viewModel: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$LoginPrimaryStateImplCopyWith<$Res>
    implements $LoginStateCopyWith<$Res> {
  factory _$$LoginPrimaryStateImplCopyWith(_$LoginPrimaryStateImpl value,
          $Res Function(_$LoginPrimaryStateImpl) then) =
      __$$LoginPrimaryStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({LoginViewModel viewModel});

  @override
  $LoginViewModelCopyWith<$Res> get viewModel;
}

/// @nodoc
class __$$LoginPrimaryStateImplCopyWithImpl<$Res>
    extends _$LoginStateCopyWithImpl<$Res, _$LoginPrimaryStateImpl>
    implements _$$LoginPrimaryStateImplCopyWith<$Res> {
  __$$LoginPrimaryStateImplCopyWithImpl(_$LoginPrimaryStateImpl _value,
      $Res Function(_$LoginPrimaryStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? viewModel = null,
  }) {
    return _then(_$LoginPrimaryStateImpl(
      viewModel: null == viewModel
          ? _value.viewModel
          : viewModel // ignore: cast_nullable_to_non_nullable
              as LoginViewModel,
    ));
  }
}

/// @nodoc

class _$LoginPrimaryStateImpl extends LoginPrimaryState {
  const _$LoginPrimaryStateImpl({this.viewModel = const LoginViewModel()})
      : super._();

  @override
  @JsonKey()
  final LoginViewModel viewModel;

  @override
  String toString() {
    return 'LoginState.loginPrimaryState(viewModel: $viewModel)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LoginPrimaryStateImpl &&
            (identical(other.viewModel, viewModel) ||
                other.viewModel == viewModel));
  }

  @override
  int get hashCode => Object.hash(runtimeType, viewModel);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$LoginPrimaryStateImplCopyWith<_$LoginPrimaryStateImpl> get copyWith =>
      __$$LoginPrimaryStateImplCopyWithImpl<_$LoginPrimaryStateImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(LoginViewModel viewModel) loginPrimaryState,
    required TResult Function(LoginViewModel viewModel, bool shouldShowLoading)
        loginLoadingState,
    required TResult Function(LoginViewModel viewModel) loginChangeFormState,
    required TResult Function(LoginViewModel viewModel) loginSuccessState,
    required TResult Function(LoginViewModel viewModel) loginPhoneVerified,
    required TResult Function(LoginViewModel viewModel) loginPhoneNotVerified,
    required TResult Function(LoginViewModel viewModel) resetPinCode,
    required TResult Function(LoginViewModel viewModel)
        loginCheckPhoneSuccessState,
    required TResult Function(LoginViewModel viewModel, BaseException exception)
        loginErrorState,
  }) {
    return loginPrimaryState(viewModel);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(LoginViewModel viewModel)? loginPrimaryState,
    TResult? Function(LoginViewModel viewModel, bool shouldShowLoading)?
        loginLoadingState,
    TResult? Function(LoginViewModel viewModel)? loginChangeFormState,
    TResult? Function(LoginViewModel viewModel)? loginSuccessState,
    TResult? Function(LoginViewModel viewModel)? loginPhoneVerified,
    TResult? Function(LoginViewModel viewModel)? loginPhoneNotVerified,
    TResult? Function(LoginViewModel viewModel)? resetPinCode,
    TResult? Function(LoginViewModel viewModel)? loginCheckPhoneSuccessState,
    TResult? Function(LoginViewModel viewModel, BaseException exception)?
        loginErrorState,
  }) {
    return loginPrimaryState?.call(viewModel);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(LoginViewModel viewModel)? loginPrimaryState,
    TResult Function(LoginViewModel viewModel, bool shouldShowLoading)?
        loginLoadingState,
    TResult Function(LoginViewModel viewModel)? loginChangeFormState,
    TResult Function(LoginViewModel viewModel)? loginSuccessState,
    TResult Function(LoginViewModel viewModel)? loginPhoneVerified,
    TResult Function(LoginViewModel viewModel)? loginPhoneNotVerified,
    TResult Function(LoginViewModel viewModel)? resetPinCode,
    TResult Function(LoginViewModel viewModel)? loginCheckPhoneSuccessState,
    TResult Function(LoginViewModel viewModel, BaseException exception)?
        loginErrorState,
    required TResult orElse(),
  }) {
    if (loginPrimaryState != null) {
      return loginPrimaryState(viewModel);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoginPrimaryState value) loginPrimaryState,
    required TResult Function(LoginLoadingState value) loginLoadingState,
    required TResult Function(LoginChangeFormState value) loginChangeFormState,
    required TResult Function(LoginSuccessState value) loginSuccessState,
    required TResult Function(LoginPhoneVerified value) loginPhoneVerified,
    required TResult Function(LoginPhoneNotVerified value)
        loginPhoneNotVerified,
    required TResult Function(ResetPinCodeState value) resetPinCode,
    required TResult Function(LoginCheckPhoneSuccessState value)
        loginCheckPhoneSuccessState,
    required TResult Function(LoginErrorState value) loginErrorState,
  }) {
    return loginPrimaryState(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(LoginPrimaryState value)? loginPrimaryState,
    TResult? Function(LoginLoadingState value)? loginLoadingState,
    TResult? Function(LoginChangeFormState value)? loginChangeFormState,
    TResult? Function(LoginSuccessState value)? loginSuccessState,
    TResult? Function(LoginPhoneVerified value)? loginPhoneVerified,
    TResult? Function(LoginPhoneNotVerified value)? loginPhoneNotVerified,
    TResult? Function(ResetPinCodeState value)? resetPinCode,
    TResult? Function(LoginCheckPhoneSuccessState value)?
        loginCheckPhoneSuccessState,
    TResult? Function(LoginErrorState value)? loginErrorState,
  }) {
    return loginPrimaryState?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoginPrimaryState value)? loginPrimaryState,
    TResult Function(LoginLoadingState value)? loginLoadingState,
    TResult Function(LoginChangeFormState value)? loginChangeFormState,
    TResult Function(LoginSuccessState value)? loginSuccessState,
    TResult Function(LoginPhoneVerified value)? loginPhoneVerified,
    TResult Function(LoginPhoneNotVerified value)? loginPhoneNotVerified,
    TResult Function(ResetPinCodeState value)? resetPinCode,
    TResult Function(LoginCheckPhoneSuccessState value)?
        loginCheckPhoneSuccessState,
    TResult Function(LoginErrorState value)? loginErrorState,
    required TResult orElse(),
  }) {
    if (loginPrimaryState != null) {
      return loginPrimaryState(this);
    }
    return orElse();
  }
}

abstract class LoginPrimaryState extends LoginState {
  const factory LoginPrimaryState({final LoginViewModel viewModel}) =
      _$LoginPrimaryStateImpl;
  const LoginPrimaryState._() : super._();

  @override
  LoginViewModel get viewModel;
  @override
  @JsonKey(ignore: true)
  _$$LoginPrimaryStateImplCopyWith<_$LoginPrimaryStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$LoginLoadingStateImplCopyWith<$Res>
    implements $LoginStateCopyWith<$Res> {
  factory _$$LoginLoadingStateImplCopyWith(_$LoginLoadingStateImpl value,
          $Res Function(_$LoginLoadingStateImpl) then) =
      __$$LoginLoadingStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({LoginViewModel viewModel, bool shouldShowLoading});

  @override
  $LoginViewModelCopyWith<$Res> get viewModel;
}

/// @nodoc
class __$$LoginLoadingStateImplCopyWithImpl<$Res>
    extends _$LoginStateCopyWithImpl<$Res, _$LoginLoadingStateImpl>
    implements _$$LoginLoadingStateImplCopyWith<$Res> {
  __$$LoginLoadingStateImplCopyWithImpl(_$LoginLoadingStateImpl _value,
      $Res Function(_$LoginLoadingStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? viewModel = null,
    Object? shouldShowLoading = null,
  }) {
    return _then(_$LoginLoadingStateImpl(
      viewModel: null == viewModel
          ? _value.viewModel
          : viewModel // ignore: cast_nullable_to_non_nullable
              as LoginViewModel,
      shouldShowLoading: null == shouldShowLoading
          ? _value.shouldShowLoading
          : shouldShowLoading // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$LoginLoadingStateImpl extends LoginLoadingState {
  const _$LoginLoadingStateImpl(
      {this.viewModel = const LoginViewModel(), this.shouldShowLoading = true})
      : super._();

  @override
  @JsonKey()
  final LoginViewModel viewModel;
  @override
  @JsonKey()
  final bool shouldShowLoading;

  @override
  String toString() {
    return 'LoginState.loginLoadingState(viewModel: $viewModel, shouldShowLoading: $shouldShowLoading)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LoginLoadingStateImpl &&
            (identical(other.viewModel, viewModel) ||
                other.viewModel == viewModel) &&
            (identical(other.shouldShowLoading, shouldShowLoading) ||
                other.shouldShowLoading == shouldShowLoading));
  }

  @override
  int get hashCode => Object.hash(runtimeType, viewModel, shouldShowLoading);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$LoginLoadingStateImplCopyWith<_$LoginLoadingStateImpl> get copyWith =>
      __$$LoginLoadingStateImplCopyWithImpl<_$LoginLoadingStateImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(LoginViewModel viewModel) loginPrimaryState,
    required TResult Function(LoginViewModel viewModel, bool shouldShowLoading)
        loginLoadingState,
    required TResult Function(LoginViewModel viewModel) loginChangeFormState,
    required TResult Function(LoginViewModel viewModel) loginSuccessState,
    required TResult Function(LoginViewModel viewModel) loginPhoneVerified,
    required TResult Function(LoginViewModel viewModel) loginPhoneNotVerified,
    required TResult Function(LoginViewModel viewModel) resetPinCode,
    required TResult Function(LoginViewModel viewModel)
        loginCheckPhoneSuccessState,
    required TResult Function(LoginViewModel viewModel, BaseException exception)
        loginErrorState,
  }) {
    return loginLoadingState(viewModel, shouldShowLoading);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(LoginViewModel viewModel)? loginPrimaryState,
    TResult? Function(LoginViewModel viewModel, bool shouldShowLoading)?
        loginLoadingState,
    TResult? Function(LoginViewModel viewModel)? loginChangeFormState,
    TResult? Function(LoginViewModel viewModel)? loginSuccessState,
    TResult? Function(LoginViewModel viewModel)? loginPhoneVerified,
    TResult? Function(LoginViewModel viewModel)? loginPhoneNotVerified,
    TResult? Function(LoginViewModel viewModel)? resetPinCode,
    TResult? Function(LoginViewModel viewModel)? loginCheckPhoneSuccessState,
    TResult? Function(LoginViewModel viewModel, BaseException exception)?
        loginErrorState,
  }) {
    return loginLoadingState?.call(viewModel, shouldShowLoading);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(LoginViewModel viewModel)? loginPrimaryState,
    TResult Function(LoginViewModel viewModel, bool shouldShowLoading)?
        loginLoadingState,
    TResult Function(LoginViewModel viewModel)? loginChangeFormState,
    TResult Function(LoginViewModel viewModel)? loginSuccessState,
    TResult Function(LoginViewModel viewModel)? loginPhoneVerified,
    TResult Function(LoginViewModel viewModel)? loginPhoneNotVerified,
    TResult Function(LoginViewModel viewModel)? resetPinCode,
    TResult Function(LoginViewModel viewModel)? loginCheckPhoneSuccessState,
    TResult Function(LoginViewModel viewModel, BaseException exception)?
        loginErrorState,
    required TResult orElse(),
  }) {
    if (loginLoadingState != null) {
      return loginLoadingState(viewModel, shouldShowLoading);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoginPrimaryState value) loginPrimaryState,
    required TResult Function(LoginLoadingState value) loginLoadingState,
    required TResult Function(LoginChangeFormState value) loginChangeFormState,
    required TResult Function(LoginSuccessState value) loginSuccessState,
    required TResult Function(LoginPhoneVerified value) loginPhoneVerified,
    required TResult Function(LoginPhoneNotVerified value)
        loginPhoneNotVerified,
    required TResult Function(ResetPinCodeState value) resetPinCode,
    required TResult Function(LoginCheckPhoneSuccessState value)
        loginCheckPhoneSuccessState,
    required TResult Function(LoginErrorState value) loginErrorState,
  }) {
    return loginLoadingState(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(LoginPrimaryState value)? loginPrimaryState,
    TResult? Function(LoginLoadingState value)? loginLoadingState,
    TResult? Function(LoginChangeFormState value)? loginChangeFormState,
    TResult? Function(LoginSuccessState value)? loginSuccessState,
    TResult? Function(LoginPhoneVerified value)? loginPhoneVerified,
    TResult? Function(LoginPhoneNotVerified value)? loginPhoneNotVerified,
    TResult? Function(ResetPinCodeState value)? resetPinCode,
    TResult? Function(LoginCheckPhoneSuccessState value)?
        loginCheckPhoneSuccessState,
    TResult? Function(LoginErrorState value)? loginErrorState,
  }) {
    return loginLoadingState?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoginPrimaryState value)? loginPrimaryState,
    TResult Function(LoginLoadingState value)? loginLoadingState,
    TResult Function(LoginChangeFormState value)? loginChangeFormState,
    TResult Function(LoginSuccessState value)? loginSuccessState,
    TResult Function(LoginPhoneVerified value)? loginPhoneVerified,
    TResult Function(LoginPhoneNotVerified value)? loginPhoneNotVerified,
    TResult Function(ResetPinCodeState value)? resetPinCode,
    TResult Function(LoginCheckPhoneSuccessState value)?
        loginCheckPhoneSuccessState,
    TResult Function(LoginErrorState value)? loginErrorState,
    required TResult orElse(),
  }) {
    if (loginLoadingState != null) {
      return loginLoadingState(this);
    }
    return orElse();
  }
}

abstract class LoginLoadingState extends LoginState {
  const factory LoginLoadingState(
      {final LoginViewModel viewModel,
      final bool shouldShowLoading}) = _$LoginLoadingStateImpl;
  const LoginLoadingState._() : super._();

  @override
  LoginViewModel get viewModel;
  bool get shouldShowLoading;
  @override
  @JsonKey(ignore: true)
  _$$LoginLoadingStateImplCopyWith<_$LoginLoadingStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$LoginChangeFormStateImplCopyWith<$Res>
    implements $LoginStateCopyWith<$Res> {
  factory _$$LoginChangeFormStateImplCopyWith(_$LoginChangeFormStateImpl value,
          $Res Function(_$LoginChangeFormStateImpl) then) =
      __$$LoginChangeFormStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({LoginViewModel viewModel});

  @override
  $LoginViewModelCopyWith<$Res> get viewModel;
}

/// @nodoc
class __$$LoginChangeFormStateImplCopyWithImpl<$Res>
    extends _$LoginStateCopyWithImpl<$Res, _$LoginChangeFormStateImpl>
    implements _$$LoginChangeFormStateImplCopyWith<$Res> {
  __$$LoginChangeFormStateImplCopyWithImpl(_$LoginChangeFormStateImpl _value,
      $Res Function(_$LoginChangeFormStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? viewModel = null,
  }) {
    return _then(_$LoginChangeFormStateImpl(
      viewModel: null == viewModel
          ? _value.viewModel
          : viewModel // ignore: cast_nullable_to_non_nullable
              as LoginViewModel,
    ));
  }
}

/// @nodoc

class _$LoginChangeFormStateImpl extends LoginChangeFormState {
  const _$LoginChangeFormStateImpl({this.viewModel = const LoginViewModel()})
      : super._();

  @override
  @JsonKey()
  final LoginViewModel viewModel;

  @override
  String toString() {
    return 'LoginState.loginChangeFormState(viewModel: $viewModel)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LoginChangeFormStateImpl &&
            (identical(other.viewModel, viewModel) ||
                other.viewModel == viewModel));
  }

  @override
  int get hashCode => Object.hash(runtimeType, viewModel);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$LoginChangeFormStateImplCopyWith<_$LoginChangeFormStateImpl>
      get copyWith =>
          __$$LoginChangeFormStateImplCopyWithImpl<_$LoginChangeFormStateImpl>(
              this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(LoginViewModel viewModel) loginPrimaryState,
    required TResult Function(LoginViewModel viewModel, bool shouldShowLoading)
        loginLoadingState,
    required TResult Function(LoginViewModel viewModel) loginChangeFormState,
    required TResult Function(LoginViewModel viewModel) loginSuccessState,
    required TResult Function(LoginViewModel viewModel) loginPhoneVerified,
    required TResult Function(LoginViewModel viewModel) loginPhoneNotVerified,
    required TResult Function(LoginViewModel viewModel) resetPinCode,
    required TResult Function(LoginViewModel viewModel)
        loginCheckPhoneSuccessState,
    required TResult Function(LoginViewModel viewModel, BaseException exception)
        loginErrorState,
  }) {
    return loginChangeFormState(viewModel);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(LoginViewModel viewModel)? loginPrimaryState,
    TResult? Function(LoginViewModel viewModel, bool shouldShowLoading)?
        loginLoadingState,
    TResult? Function(LoginViewModel viewModel)? loginChangeFormState,
    TResult? Function(LoginViewModel viewModel)? loginSuccessState,
    TResult? Function(LoginViewModel viewModel)? loginPhoneVerified,
    TResult? Function(LoginViewModel viewModel)? loginPhoneNotVerified,
    TResult? Function(LoginViewModel viewModel)? resetPinCode,
    TResult? Function(LoginViewModel viewModel)? loginCheckPhoneSuccessState,
    TResult? Function(LoginViewModel viewModel, BaseException exception)?
        loginErrorState,
  }) {
    return loginChangeFormState?.call(viewModel);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(LoginViewModel viewModel)? loginPrimaryState,
    TResult Function(LoginViewModel viewModel, bool shouldShowLoading)?
        loginLoadingState,
    TResult Function(LoginViewModel viewModel)? loginChangeFormState,
    TResult Function(LoginViewModel viewModel)? loginSuccessState,
    TResult Function(LoginViewModel viewModel)? loginPhoneVerified,
    TResult Function(LoginViewModel viewModel)? loginPhoneNotVerified,
    TResult Function(LoginViewModel viewModel)? resetPinCode,
    TResult Function(LoginViewModel viewModel)? loginCheckPhoneSuccessState,
    TResult Function(LoginViewModel viewModel, BaseException exception)?
        loginErrorState,
    required TResult orElse(),
  }) {
    if (loginChangeFormState != null) {
      return loginChangeFormState(viewModel);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoginPrimaryState value) loginPrimaryState,
    required TResult Function(LoginLoadingState value) loginLoadingState,
    required TResult Function(LoginChangeFormState value) loginChangeFormState,
    required TResult Function(LoginSuccessState value) loginSuccessState,
    required TResult Function(LoginPhoneVerified value) loginPhoneVerified,
    required TResult Function(LoginPhoneNotVerified value)
        loginPhoneNotVerified,
    required TResult Function(ResetPinCodeState value) resetPinCode,
    required TResult Function(LoginCheckPhoneSuccessState value)
        loginCheckPhoneSuccessState,
    required TResult Function(LoginErrorState value) loginErrorState,
  }) {
    return loginChangeFormState(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(LoginPrimaryState value)? loginPrimaryState,
    TResult? Function(LoginLoadingState value)? loginLoadingState,
    TResult? Function(LoginChangeFormState value)? loginChangeFormState,
    TResult? Function(LoginSuccessState value)? loginSuccessState,
    TResult? Function(LoginPhoneVerified value)? loginPhoneVerified,
    TResult? Function(LoginPhoneNotVerified value)? loginPhoneNotVerified,
    TResult? Function(ResetPinCodeState value)? resetPinCode,
    TResult? Function(LoginCheckPhoneSuccessState value)?
        loginCheckPhoneSuccessState,
    TResult? Function(LoginErrorState value)? loginErrorState,
  }) {
    return loginChangeFormState?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoginPrimaryState value)? loginPrimaryState,
    TResult Function(LoginLoadingState value)? loginLoadingState,
    TResult Function(LoginChangeFormState value)? loginChangeFormState,
    TResult Function(LoginSuccessState value)? loginSuccessState,
    TResult Function(LoginPhoneVerified value)? loginPhoneVerified,
    TResult Function(LoginPhoneNotVerified value)? loginPhoneNotVerified,
    TResult Function(ResetPinCodeState value)? resetPinCode,
    TResult Function(LoginCheckPhoneSuccessState value)?
        loginCheckPhoneSuccessState,
    TResult Function(LoginErrorState value)? loginErrorState,
    required TResult orElse(),
  }) {
    if (loginChangeFormState != null) {
      return loginChangeFormState(this);
    }
    return orElse();
  }
}

abstract class LoginChangeFormState extends LoginState {
  const factory LoginChangeFormState({final LoginViewModel viewModel}) =
      _$LoginChangeFormStateImpl;
  const LoginChangeFormState._() : super._();

  @override
  LoginViewModel get viewModel;
  @override
  @JsonKey(ignore: true)
  _$$LoginChangeFormStateImplCopyWith<_$LoginChangeFormStateImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$LoginSuccessStateImplCopyWith<$Res>
    implements $LoginStateCopyWith<$Res> {
  factory _$$LoginSuccessStateImplCopyWith(_$LoginSuccessStateImpl value,
          $Res Function(_$LoginSuccessStateImpl) then) =
      __$$LoginSuccessStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({LoginViewModel viewModel});

  @override
  $LoginViewModelCopyWith<$Res> get viewModel;
}

/// @nodoc
class __$$LoginSuccessStateImplCopyWithImpl<$Res>
    extends _$LoginStateCopyWithImpl<$Res, _$LoginSuccessStateImpl>
    implements _$$LoginSuccessStateImplCopyWith<$Res> {
  __$$LoginSuccessStateImplCopyWithImpl(_$LoginSuccessStateImpl _value,
      $Res Function(_$LoginSuccessStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? viewModel = null,
  }) {
    return _then(_$LoginSuccessStateImpl(
      viewModel: null == viewModel
          ? _value.viewModel
          : viewModel // ignore: cast_nullable_to_non_nullable
              as LoginViewModel,
    ));
  }
}

/// @nodoc

class _$LoginSuccessStateImpl extends LoginSuccessState {
  const _$LoginSuccessStateImpl({this.viewModel = const LoginViewModel()})
      : super._();

  @override
  @JsonKey()
  final LoginViewModel viewModel;

  @override
  String toString() {
    return 'LoginState.loginSuccessState(viewModel: $viewModel)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LoginSuccessStateImpl &&
            (identical(other.viewModel, viewModel) ||
                other.viewModel == viewModel));
  }

  @override
  int get hashCode => Object.hash(runtimeType, viewModel);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$LoginSuccessStateImplCopyWith<_$LoginSuccessStateImpl> get copyWith =>
      __$$LoginSuccessStateImplCopyWithImpl<_$LoginSuccessStateImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(LoginViewModel viewModel) loginPrimaryState,
    required TResult Function(LoginViewModel viewModel, bool shouldShowLoading)
        loginLoadingState,
    required TResult Function(LoginViewModel viewModel) loginChangeFormState,
    required TResult Function(LoginViewModel viewModel) loginSuccessState,
    required TResult Function(LoginViewModel viewModel) loginPhoneVerified,
    required TResult Function(LoginViewModel viewModel) loginPhoneNotVerified,
    required TResult Function(LoginViewModel viewModel) resetPinCode,
    required TResult Function(LoginViewModel viewModel)
        loginCheckPhoneSuccessState,
    required TResult Function(LoginViewModel viewModel, BaseException exception)
        loginErrorState,
  }) {
    return loginSuccessState(viewModel);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(LoginViewModel viewModel)? loginPrimaryState,
    TResult? Function(LoginViewModel viewModel, bool shouldShowLoading)?
        loginLoadingState,
    TResult? Function(LoginViewModel viewModel)? loginChangeFormState,
    TResult? Function(LoginViewModel viewModel)? loginSuccessState,
    TResult? Function(LoginViewModel viewModel)? loginPhoneVerified,
    TResult? Function(LoginViewModel viewModel)? loginPhoneNotVerified,
    TResult? Function(LoginViewModel viewModel)? resetPinCode,
    TResult? Function(LoginViewModel viewModel)? loginCheckPhoneSuccessState,
    TResult? Function(LoginViewModel viewModel, BaseException exception)?
        loginErrorState,
  }) {
    return loginSuccessState?.call(viewModel);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(LoginViewModel viewModel)? loginPrimaryState,
    TResult Function(LoginViewModel viewModel, bool shouldShowLoading)?
        loginLoadingState,
    TResult Function(LoginViewModel viewModel)? loginChangeFormState,
    TResult Function(LoginViewModel viewModel)? loginSuccessState,
    TResult Function(LoginViewModel viewModel)? loginPhoneVerified,
    TResult Function(LoginViewModel viewModel)? loginPhoneNotVerified,
    TResult Function(LoginViewModel viewModel)? resetPinCode,
    TResult Function(LoginViewModel viewModel)? loginCheckPhoneSuccessState,
    TResult Function(LoginViewModel viewModel, BaseException exception)?
        loginErrorState,
    required TResult orElse(),
  }) {
    if (loginSuccessState != null) {
      return loginSuccessState(viewModel);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoginPrimaryState value) loginPrimaryState,
    required TResult Function(LoginLoadingState value) loginLoadingState,
    required TResult Function(LoginChangeFormState value) loginChangeFormState,
    required TResult Function(LoginSuccessState value) loginSuccessState,
    required TResult Function(LoginPhoneVerified value) loginPhoneVerified,
    required TResult Function(LoginPhoneNotVerified value)
        loginPhoneNotVerified,
    required TResult Function(ResetPinCodeState value) resetPinCode,
    required TResult Function(LoginCheckPhoneSuccessState value)
        loginCheckPhoneSuccessState,
    required TResult Function(LoginErrorState value) loginErrorState,
  }) {
    return loginSuccessState(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(LoginPrimaryState value)? loginPrimaryState,
    TResult? Function(LoginLoadingState value)? loginLoadingState,
    TResult? Function(LoginChangeFormState value)? loginChangeFormState,
    TResult? Function(LoginSuccessState value)? loginSuccessState,
    TResult? Function(LoginPhoneVerified value)? loginPhoneVerified,
    TResult? Function(LoginPhoneNotVerified value)? loginPhoneNotVerified,
    TResult? Function(ResetPinCodeState value)? resetPinCode,
    TResult? Function(LoginCheckPhoneSuccessState value)?
        loginCheckPhoneSuccessState,
    TResult? Function(LoginErrorState value)? loginErrorState,
  }) {
    return loginSuccessState?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoginPrimaryState value)? loginPrimaryState,
    TResult Function(LoginLoadingState value)? loginLoadingState,
    TResult Function(LoginChangeFormState value)? loginChangeFormState,
    TResult Function(LoginSuccessState value)? loginSuccessState,
    TResult Function(LoginPhoneVerified value)? loginPhoneVerified,
    TResult Function(LoginPhoneNotVerified value)? loginPhoneNotVerified,
    TResult Function(ResetPinCodeState value)? resetPinCode,
    TResult Function(LoginCheckPhoneSuccessState value)?
        loginCheckPhoneSuccessState,
    TResult Function(LoginErrorState value)? loginErrorState,
    required TResult orElse(),
  }) {
    if (loginSuccessState != null) {
      return loginSuccessState(this);
    }
    return orElse();
  }
}

abstract class LoginSuccessState extends LoginState {
  const factory LoginSuccessState({final LoginViewModel viewModel}) =
      _$LoginSuccessStateImpl;
  const LoginSuccessState._() : super._();

  @override
  LoginViewModel get viewModel;
  @override
  @JsonKey(ignore: true)
  _$$LoginSuccessStateImplCopyWith<_$LoginSuccessStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$LoginPhoneVerifiedImplCopyWith<$Res>
    implements $LoginStateCopyWith<$Res> {
  factory _$$LoginPhoneVerifiedImplCopyWith(_$LoginPhoneVerifiedImpl value,
          $Res Function(_$LoginPhoneVerifiedImpl) then) =
      __$$LoginPhoneVerifiedImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({LoginViewModel viewModel});

  @override
  $LoginViewModelCopyWith<$Res> get viewModel;
}

/// @nodoc
class __$$LoginPhoneVerifiedImplCopyWithImpl<$Res>
    extends _$LoginStateCopyWithImpl<$Res, _$LoginPhoneVerifiedImpl>
    implements _$$LoginPhoneVerifiedImplCopyWith<$Res> {
  __$$LoginPhoneVerifiedImplCopyWithImpl(_$LoginPhoneVerifiedImpl _value,
      $Res Function(_$LoginPhoneVerifiedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? viewModel = null,
  }) {
    return _then(_$LoginPhoneVerifiedImpl(
      viewModel: null == viewModel
          ? _value.viewModel
          : viewModel // ignore: cast_nullable_to_non_nullable
              as LoginViewModel,
    ));
  }
}

/// @nodoc

class _$LoginPhoneVerifiedImpl extends LoginPhoneVerified {
  const _$LoginPhoneVerifiedImpl({this.viewModel = const LoginViewModel()})
      : super._();

  @override
  @JsonKey()
  final LoginViewModel viewModel;

  @override
  String toString() {
    return 'LoginState.loginPhoneVerified(viewModel: $viewModel)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LoginPhoneVerifiedImpl &&
            (identical(other.viewModel, viewModel) ||
                other.viewModel == viewModel));
  }

  @override
  int get hashCode => Object.hash(runtimeType, viewModel);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$LoginPhoneVerifiedImplCopyWith<_$LoginPhoneVerifiedImpl> get copyWith =>
      __$$LoginPhoneVerifiedImplCopyWithImpl<_$LoginPhoneVerifiedImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(LoginViewModel viewModel) loginPrimaryState,
    required TResult Function(LoginViewModel viewModel, bool shouldShowLoading)
        loginLoadingState,
    required TResult Function(LoginViewModel viewModel) loginChangeFormState,
    required TResult Function(LoginViewModel viewModel) loginSuccessState,
    required TResult Function(LoginViewModel viewModel) loginPhoneVerified,
    required TResult Function(LoginViewModel viewModel) loginPhoneNotVerified,
    required TResult Function(LoginViewModel viewModel) resetPinCode,
    required TResult Function(LoginViewModel viewModel)
        loginCheckPhoneSuccessState,
    required TResult Function(LoginViewModel viewModel, BaseException exception)
        loginErrorState,
  }) {
    return loginPhoneVerified(viewModel);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(LoginViewModel viewModel)? loginPrimaryState,
    TResult? Function(LoginViewModel viewModel, bool shouldShowLoading)?
        loginLoadingState,
    TResult? Function(LoginViewModel viewModel)? loginChangeFormState,
    TResult? Function(LoginViewModel viewModel)? loginSuccessState,
    TResult? Function(LoginViewModel viewModel)? loginPhoneVerified,
    TResult? Function(LoginViewModel viewModel)? loginPhoneNotVerified,
    TResult? Function(LoginViewModel viewModel)? resetPinCode,
    TResult? Function(LoginViewModel viewModel)? loginCheckPhoneSuccessState,
    TResult? Function(LoginViewModel viewModel, BaseException exception)?
        loginErrorState,
  }) {
    return loginPhoneVerified?.call(viewModel);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(LoginViewModel viewModel)? loginPrimaryState,
    TResult Function(LoginViewModel viewModel, bool shouldShowLoading)?
        loginLoadingState,
    TResult Function(LoginViewModel viewModel)? loginChangeFormState,
    TResult Function(LoginViewModel viewModel)? loginSuccessState,
    TResult Function(LoginViewModel viewModel)? loginPhoneVerified,
    TResult Function(LoginViewModel viewModel)? loginPhoneNotVerified,
    TResult Function(LoginViewModel viewModel)? resetPinCode,
    TResult Function(LoginViewModel viewModel)? loginCheckPhoneSuccessState,
    TResult Function(LoginViewModel viewModel, BaseException exception)?
        loginErrorState,
    required TResult orElse(),
  }) {
    if (loginPhoneVerified != null) {
      return loginPhoneVerified(viewModel);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoginPrimaryState value) loginPrimaryState,
    required TResult Function(LoginLoadingState value) loginLoadingState,
    required TResult Function(LoginChangeFormState value) loginChangeFormState,
    required TResult Function(LoginSuccessState value) loginSuccessState,
    required TResult Function(LoginPhoneVerified value) loginPhoneVerified,
    required TResult Function(LoginPhoneNotVerified value)
        loginPhoneNotVerified,
    required TResult Function(ResetPinCodeState value) resetPinCode,
    required TResult Function(LoginCheckPhoneSuccessState value)
        loginCheckPhoneSuccessState,
    required TResult Function(LoginErrorState value) loginErrorState,
  }) {
    return loginPhoneVerified(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(LoginPrimaryState value)? loginPrimaryState,
    TResult? Function(LoginLoadingState value)? loginLoadingState,
    TResult? Function(LoginChangeFormState value)? loginChangeFormState,
    TResult? Function(LoginSuccessState value)? loginSuccessState,
    TResult? Function(LoginPhoneVerified value)? loginPhoneVerified,
    TResult? Function(LoginPhoneNotVerified value)? loginPhoneNotVerified,
    TResult? Function(ResetPinCodeState value)? resetPinCode,
    TResult? Function(LoginCheckPhoneSuccessState value)?
        loginCheckPhoneSuccessState,
    TResult? Function(LoginErrorState value)? loginErrorState,
  }) {
    return loginPhoneVerified?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoginPrimaryState value)? loginPrimaryState,
    TResult Function(LoginLoadingState value)? loginLoadingState,
    TResult Function(LoginChangeFormState value)? loginChangeFormState,
    TResult Function(LoginSuccessState value)? loginSuccessState,
    TResult Function(LoginPhoneVerified value)? loginPhoneVerified,
    TResult Function(LoginPhoneNotVerified value)? loginPhoneNotVerified,
    TResult Function(ResetPinCodeState value)? resetPinCode,
    TResult Function(LoginCheckPhoneSuccessState value)?
        loginCheckPhoneSuccessState,
    TResult Function(LoginErrorState value)? loginErrorState,
    required TResult orElse(),
  }) {
    if (loginPhoneVerified != null) {
      return loginPhoneVerified(this);
    }
    return orElse();
  }
}

abstract class LoginPhoneVerified extends LoginState {
  const factory LoginPhoneVerified({final LoginViewModel viewModel}) =
      _$LoginPhoneVerifiedImpl;
  const LoginPhoneVerified._() : super._();

  @override
  LoginViewModel get viewModel;
  @override
  @JsonKey(ignore: true)
  _$$LoginPhoneVerifiedImplCopyWith<_$LoginPhoneVerifiedImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$LoginPhoneNotVerifiedImplCopyWith<$Res>
    implements $LoginStateCopyWith<$Res> {
  factory _$$LoginPhoneNotVerifiedImplCopyWith(
          _$LoginPhoneNotVerifiedImpl value,
          $Res Function(_$LoginPhoneNotVerifiedImpl) then) =
      __$$LoginPhoneNotVerifiedImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({LoginViewModel viewModel});

  @override
  $LoginViewModelCopyWith<$Res> get viewModel;
}

/// @nodoc
class __$$LoginPhoneNotVerifiedImplCopyWithImpl<$Res>
    extends _$LoginStateCopyWithImpl<$Res, _$LoginPhoneNotVerifiedImpl>
    implements _$$LoginPhoneNotVerifiedImplCopyWith<$Res> {
  __$$LoginPhoneNotVerifiedImplCopyWithImpl(_$LoginPhoneNotVerifiedImpl _value,
      $Res Function(_$LoginPhoneNotVerifiedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? viewModel = null,
  }) {
    return _then(_$LoginPhoneNotVerifiedImpl(
      viewModel: null == viewModel
          ? _value.viewModel
          : viewModel // ignore: cast_nullable_to_non_nullable
              as LoginViewModel,
    ));
  }
}

/// @nodoc

class _$LoginPhoneNotVerifiedImpl extends LoginPhoneNotVerified {
  const _$LoginPhoneNotVerifiedImpl({this.viewModel = const LoginViewModel()})
      : super._();

  @override
  @JsonKey()
  final LoginViewModel viewModel;

  @override
  String toString() {
    return 'LoginState.loginPhoneNotVerified(viewModel: $viewModel)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LoginPhoneNotVerifiedImpl &&
            (identical(other.viewModel, viewModel) ||
                other.viewModel == viewModel));
  }

  @override
  int get hashCode => Object.hash(runtimeType, viewModel);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$LoginPhoneNotVerifiedImplCopyWith<_$LoginPhoneNotVerifiedImpl>
      get copyWith => __$$LoginPhoneNotVerifiedImplCopyWithImpl<
          _$LoginPhoneNotVerifiedImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(LoginViewModel viewModel) loginPrimaryState,
    required TResult Function(LoginViewModel viewModel, bool shouldShowLoading)
        loginLoadingState,
    required TResult Function(LoginViewModel viewModel) loginChangeFormState,
    required TResult Function(LoginViewModel viewModel) loginSuccessState,
    required TResult Function(LoginViewModel viewModel) loginPhoneVerified,
    required TResult Function(LoginViewModel viewModel) loginPhoneNotVerified,
    required TResult Function(LoginViewModel viewModel) resetPinCode,
    required TResult Function(LoginViewModel viewModel)
        loginCheckPhoneSuccessState,
    required TResult Function(LoginViewModel viewModel, BaseException exception)
        loginErrorState,
  }) {
    return loginPhoneNotVerified(viewModel);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(LoginViewModel viewModel)? loginPrimaryState,
    TResult? Function(LoginViewModel viewModel, bool shouldShowLoading)?
        loginLoadingState,
    TResult? Function(LoginViewModel viewModel)? loginChangeFormState,
    TResult? Function(LoginViewModel viewModel)? loginSuccessState,
    TResult? Function(LoginViewModel viewModel)? loginPhoneVerified,
    TResult? Function(LoginViewModel viewModel)? loginPhoneNotVerified,
    TResult? Function(LoginViewModel viewModel)? resetPinCode,
    TResult? Function(LoginViewModel viewModel)? loginCheckPhoneSuccessState,
    TResult? Function(LoginViewModel viewModel, BaseException exception)?
        loginErrorState,
  }) {
    return loginPhoneNotVerified?.call(viewModel);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(LoginViewModel viewModel)? loginPrimaryState,
    TResult Function(LoginViewModel viewModel, bool shouldShowLoading)?
        loginLoadingState,
    TResult Function(LoginViewModel viewModel)? loginChangeFormState,
    TResult Function(LoginViewModel viewModel)? loginSuccessState,
    TResult Function(LoginViewModel viewModel)? loginPhoneVerified,
    TResult Function(LoginViewModel viewModel)? loginPhoneNotVerified,
    TResult Function(LoginViewModel viewModel)? resetPinCode,
    TResult Function(LoginViewModel viewModel)? loginCheckPhoneSuccessState,
    TResult Function(LoginViewModel viewModel, BaseException exception)?
        loginErrorState,
    required TResult orElse(),
  }) {
    if (loginPhoneNotVerified != null) {
      return loginPhoneNotVerified(viewModel);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoginPrimaryState value) loginPrimaryState,
    required TResult Function(LoginLoadingState value) loginLoadingState,
    required TResult Function(LoginChangeFormState value) loginChangeFormState,
    required TResult Function(LoginSuccessState value) loginSuccessState,
    required TResult Function(LoginPhoneVerified value) loginPhoneVerified,
    required TResult Function(LoginPhoneNotVerified value)
        loginPhoneNotVerified,
    required TResult Function(ResetPinCodeState value) resetPinCode,
    required TResult Function(LoginCheckPhoneSuccessState value)
        loginCheckPhoneSuccessState,
    required TResult Function(LoginErrorState value) loginErrorState,
  }) {
    return loginPhoneNotVerified(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(LoginPrimaryState value)? loginPrimaryState,
    TResult? Function(LoginLoadingState value)? loginLoadingState,
    TResult? Function(LoginChangeFormState value)? loginChangeFormState,
    TResult? Function(LoginSuccessState value)? loginSuccessState,
    TResult? Function(LoginPhoneVerified value)? loginPhoneVerified,
    TResult? Function(LoginPhoneNotVerified value)? loginPhoneNotVerified,
    TResult? Function(ResetPinCodeState value)? resetPinCode,
    TResult? Function(LoginCheckPhoneSuccessState value)?
        loginCheckPhoneSuccessState,
    TResult? Function(LoginErrorState value)? loginErrorState,
  }) {
    return loginPhoneNotVerified?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoginPrimaryState value)? loginPrimaryState,
    TResult Function(LoginLoadingState value)? loginLoadingState,
    TResult Function(LoginChangeFormState value)? loginChangeFormState,
    TResult Function(LoginSuccessState value)? loginSuccessState,
    TResult Function(LoginPhoneVerified value)? loginPhoneVerified,
    TResult Function(LoginPhoneNotVerified value)? loginPhoneNotVerified,
    TResult Function(ResetPinCodeState value)? resetPinCode,
    TResult Function(LoginCheckPhoneSuccessState value)?
        loginCheckPhoneSuccessState,
    TResult Function(LoginErrorState value)? loginErrorState,
    required TResult orElse(),
  }) {
    if (loginPhoneNotVerified != null) {
      return loginPhoneNotVerified(this);
    }
    return orElse();
  }
}

abstract class LoginPhoneNotVerified extends LoginState {
  const factory LoginPhoneNotVerified({final LoginViewModel viewModel}) =
      _$LoginPhoneNotVerifiedImpl;
  const LoginPhoneNotVerified._() : super._();

  @override
  LoginViewModel get viewModel;
  @override
  @JsonKey(ignore: true)
  _$$LoginPhoneNotVerifiedImplCopyWith<_$LoginPhoneNotVerifiedImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ResetPinCodeStateImplCopyWith<$Res>
    implements $LoginStateCopyWith<$Res> {
  factory _$$ResetPinCodeStateImplCopyWith(_$ResetPinCodeStateImpl value,
          $Res Function(_$ResetPinCodeStateImpl) then) =
      __$$ResetPinCodeStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({LoginViewModel viewModel});

  @override
  $LoginViewModelCopyWith<$Res> get viewModel;
}

/// @nodoc
class __$$ResetPinCodeStateImplCopyWithImpl<$Res>
    extends _$LoginStateCopyWithImpl<$Res, _$ResetPinCodeStateImpl>
    implements _$$ResetPinCodeStateImplCopyWith<$Res> {
  __$$ResetPinCodeStateImplCopyWithImpl(_$ResetPinCodeStateImpl _value,
      $Res Function(_$ResetPinCodeStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? viewModel = null,
  }) {
    return _then(_$ResetPinCodeStateImpl(
      viewModel: null == viewModel
          ? _value.viewModel
          : viewModel // ignore: cast_nullable_to_non_nullable
              as LoginViewModel,
    ));
  }
}

/// @nodoc

class _$ResetPinCodeStateImpl extends ResetPinCodeState {
  const _$ResetPinCodeStateImpl({this.viewModel = const LoginViewModel()})
      : super._();

  @override
  @JsonKey()
  final LoginViewModel viewModel;

  @override
  String toString() {
    return 'LoginState.resetPinCode(viewModel: $viewModel)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ResetPinCodeStateImpl &&
            (identical(other.viewModel, viewModel) ||
                other.viewModel == viewModel));
  }

  @override
  int get hashCode => Object.hash(runtimeType, viewModel);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ResetPinCodeStateImplCopyWith<_$ResetPinCodeStateImpl> get copyWith =>
      __$$ResetPinCodeStateImplCopyWithImpl<_$ResetPinCodeStateImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(LoginViewModel viewModel) loginPrimaryState,
    required TResult Function(LoginViewModel viewModel, bool shouldShowLoading)
        loginLoadingState,
    required TResult Function(LoginViewModel viewModel) loginChangeFormState,
    required TResult Function(LoginViewModel viewModel) loginSuccessState,
    required TResult Function(LoginViewModel viewModel) loginPhoneVerified,
    required TResult Function(LoginViewModel viewModel) loginPhoneNotVerified,
    required TResult Function(LoginViewModel viewModel) resetPinCode,
    required TResult Function(LoginViewModel viewModel)
        loginCheckPhoneSuccessState,
    required TResult Function(LoginViewModel viewModel, BaseException exception)
        loginErrorState,
  }) {
    return resetPinCode(viewModel);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(LoginViewModel viewModel)? loginPrimaryState,
    TResult? Function(LoginViewModel viewModel, bool shouldShowLoading)?
        loginLoadingState,
    TResult? Function(LoginViewModel viewModel)? loginChangeFormState,
    TResult? Function(LoginViewModel viewModel)? loginSuccessState,
    TResult? Function(LoginViewModel viewModel)? loginPhoneVerified,
    TResult? Function(LoginViewModel viewModel)? loginPhoneNotVerified,
    TResult? Function(LoginViewModel viewModel)? resetPinCode,
    TResult? Function(LoginViewModel viewModel)? loginCheckPhoneSuccessState,
    TResult? Function(LoginViewModel viewModel, BaseException exception)?
        loginErrorState,
  }) {
    return resetPinCode?.call(viewModel);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(LoginViewModel viewModel)? loginPrimaryState,
    TResult Function(LoginViewModel viewModel, bool shouldShowLoading)?
        loginLoadingState,
    TResult Function(LoginViewModel viewModel)? loginChangeFormState,
    TResult Function(LoginViewModel viewModel)? loginSuccessState,
    TResult Function(LoginViewModel viewModel)? loginPhoneVerified,
    TResult Function(LoginViewModel viewModel)? loginPhoneNotVerified,
    TResult Function(LoginViewModel viewModel)? resetPinCode,
    TResult Function(LoginViewModel viewModel)? loginCheckPhoneSuccessState,
    TResult Function(LoginViewModel viewModel, BaseException exception)?
        loginErrorState,
    required TResult orElse(),
  }) {
    if (resetPinCode != null) {
      return resetPinCode(viewModel);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoginPrimaryState value) loginPrimaryState,
    required TResult Function(LoginLoadingState value) loginLoadingState,
    required TResult Function(LoginChangeFormState value) loginChangeFormState,
    required TResult Function(LoginSuccessState value) loginSuccessState,
    required TResult Function(LoginPhoneVerified value) loginPhoneVerified,
    required TResult Function(LoginPhoneNotVerified value)
        loginPhoneNotVerified,
    required TResult Function(ResetPinCodeState value) resetPinCode,
    required TResult Function(LoginCheckPhoneSuccessState value)
        loginCheckPhoneSuccessState,
    required TResult Function(LoginErrorState value) loginErrorState,
  }) {
    return resetPinCode(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(LoginPrimaryState value)? loginPrimaryState,
    TResult? Function(LoginLoadingState value)? loginLoadingState,
    TResult? Function(LoginChangeFormState value)? loginChangeFormState,
    TResult? Function(LoginSuccessState value)? loginSuccessState,
    TResult? Function(LoginPhoneVerified value)? loginPhoneVerified,
    TResult? Function(LoginPhoneNotVerified value)? loginPhoneNotVerified,
    TResult? Function(ResetPinCodeState value)? resetPinCode,
    TResult? Function(LoginCheckPhoneSuccessState value)?
        loginCheckPhoneSuccessState,
    TResult? Function(LoginErrorState value)? loginErrorState,
  }) {
    return resetPinCode?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoginPrimaryState value)? loginPrimaryState,
    TResult Function(LoginLoadingState value)? loginLoadingState,
    TResult Function(LoginChangeFormState value)? loginChangeFormState,
    TResult Function(LoginSuccessState value)? loginSuccessState,
    TResult Function(LoginPhoneVerified value)? loginPhoneVerified,
    TResult Function(LoginPhoneNotVerified value)? loginPhoneNotVerified,
    TResult Function(ResetPinCodeState value)? resetPinCode,
    TResult Function(LoginCheckPhoneSuccessState value)?
        loginCheckPhoneSuccessState,
    TResult Function(LoginErrorState value)? loginErrorState,
    required TResult orElse(),
  }) {
    if (resetPinCode != null) {
      return resetPinCode(this);
    }
    return orElse();
  }
}

abstract class ResetPinCodeState extends LoginState {
  const factory ResetPinCodeState({final LoginViewModel viewModel}) =
      _$ResetPinCodeStateImpl;
  const ResetPinCodeState._() : super._();

  @override
  LoginViewModel get viewModel;
  @override
  @JsonKey(ignore: true)
  _$$ResetPinCodeStateImplCopyWith<_$ResetPinCodeStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$LoginCheckPhoneSuccessStateImplCopyWith<$Res>
    implements $LoginStateCopyWith<$Res> {
  factory _$$LoginCheckPhoneSuccessStateImplCopyWith(
          _$LoginCheckPhoneSuccessStateImpl value,
          $Res Function(_$LoginCheckPhoneSuccessStateImpl) then) =
      __$$LoginCheckPhoneSuccessStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({LoginViewModel viewModel});

  @override
  $LoginViewModelCopyWith<$Res> get viewModel;
}

/// @nodoc
class __$$LoginCheckPhoneSuccessStateImplCopyWithImpl<$Res>
    extends _$LoginStateCopyWithImpl<$Res, _$LoginCheckPhoneSuccessStateImpl>
    implements _$$LoginCheckPhoneSuccessStateImplCopyWith<$Res> {
  __$$LoginCheckPhoneSuccessStateImplCopyWithImpl(
      _$LoginCheckPhoneSuccessStateImpl _value,
      $Res Function(_$LoginCheckPhoneSuccessStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? viewModel = null,
  }) {
    return _then(_$LoginCheckPhoneSuccessStateImpl(
      viewModel: null == viewModel
          ? _value.viewModel
          : viewModel // ignore: cast_nullable_to_non_nullable
              as LoginViewModel,
    ));
  }
}

/// @nodoc

class _$LoginCheckPhoneSuccessStateImpl extends LoginCheckPhoneSuccessState {
  const _$LoginCheckPhoneSuccessStateImpl(
      {this.viewModel = const LoginViewModel()})
      : super._();

  @override
  @JsonKey()
  final LoginViewModel viewModel;

  @override
  String toString() {
    return 'LoginState.loginCheckPhoneSuccessState(viewModel: $viewModel)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LoginCheckPhoneSuccessStateImpl &&
            (identical(other.viewModel, viewModel) ||
                other.viewModel == viewModel));
  }

  @override
  int get hashCode => Object.hash(runtimeType, viewModel);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$LoginCheckPhoneSuccessStateImplCopyWith<_$LoginCheckPhoneSuccessStateImpl>
      get copyWith => __$$LoginCheckPhoneSuccessStateImplCopyWithImpl<
          _$LoginCheckPhoneSuccessStateImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(LoginViewModel viewModel) loginPrimaryState,
    required TResult Function(LoginViewModel viewModel, bool shouldShowLoading)
        loginLoadingState,
    required TResult Function(LoginViewModel viewModel) loginChangeFormState,
    required TResult Function(LoginViewModel viewModel) loginSuccessState,
    required TResult Function(LoginViewModel viewModel) loginPhoneVerified,
    required TResult Function(LoginViewModel viewModel) loginPhoneNotVerified,
    required TResult Function(LoginViewModel viewModel) resetPinCode,
    required TResult Function(LoginViewModel viewModel)
        loginCheckPhoneSuccessState,
    required TResult Function(LoginViewModel viewModel, BaseException exception)
        loginErrorState,
  }) {
    return loginCheckPhoneSuccessState(viewModel);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(LoginViewModel viewModel)? loginPrimaryState,
    TResult? Function(LoginViewModel viewModel, bool shouldShowLoading)?
        loginLoadingState,
    TResult? Function(LoginViewModel viewModel)? loginChangeFormState,
    TResult? Function(LoginViewModel viewModel)? loginSuccessState,
    TResult? Function(LoginViewModel viewModel)? loginPhoneVerified,
    TResult? Function(LoginViewModel viewModel)? loginPhoneNotVerified,
    TResult? Function(LoginViewModel viewModel)? resetPinCode,
    TResult? Function(LoginViewModel viewModel)? loginCheckPhoneSuccessState,
    TResult? Function(LoginViewModel viewModel, BaseException exception)?
        loginErrorState,
  }) {
    return loginCheckPhoneSuccessState?.call(viewModel);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(LoginViewModel viewModel)? loginPrimaryState,
    TResult Function(LoginViewModel viewModel, bool shouldShowLoading)?
        loginLoadingState,
    TResult Function(LoginViewModel viewModel)? loginChangeFormState,
    TResult Function(LoginViewModel viewModel)? loginSuccessState,
    TResult Function(LoginViewModel viewModel)? loginPhoneVerified,
    TResult Function(LoginViewModel viewModel)? loginPhoneNotVerified,
    TResult Function(LoginViewModel viewModel)? resetPinCode,
    TResult Function(LoginViewModel viewModel)? loginCheckPhoneSuccessState,
    TResult Function(LoginViewModel viewModel, BaseException exception)?
        loginErrorState,
    required TResult orElse(),
  }) {
    if (loginCheckPhoneSuccessState != null) {
      return loginCheckPhoneSuccessState(viewModel);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoginPrimaryState value) loginPrimaryState,
    required TResult Function(LoginLoadingState value) loginLoadingState,
    required TResult Function(LoginChangeFormState value) loginChangeFormState,
    required TResult Function(LoginSuccessState value) loginSuccessState,
    required TResult Function(LoginPhoneVerified value) loginPhoneVerified,
    required TResult Function(LoginPhoneNotVerified value)
        loginPhoneNotVerified,
    required TResult Function(ResetPinCodeState value) resetPinCode,
    required TResult Function(LoginCheckPhoneSuccessState value)
        loginCheckPhoneSuccessState,
    required TResult Function(LoginErrorState value) loginErrorState,
  }) {
    return loginCheckPhoneSuccessState(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(LoginPrimaryState value)? loginPrimaryState,
    TResult? Function(LoginLoadingState value)? loginLoadingState,
    TResult? Function(LoginChangeFormState value)? loginChangeFormState,
    TResult? Function(LoginSuccessState value)? loginSuccessState,
    TResult? Function(LoginPhoneVerified value)? loginPhoneVerified,
    TResult? Function(LoginPhoneNotVerified value)? loginPhoneNotVerified,
    TResult? Function(ResetPinCodeState value)? resetPinCode,
    TResult? Function(LoginCheckPhoneSuccessState value)?
        loginCheckPhoneSuccessState,
    TResult? Function(LoginErrorState value)? loginErrorState,
  }) {
    return loginCheckPhoneSuccessState?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoginPrimaryState value)? loginPrimaryState,
    TResult Function(LoginLoadingState value)? loginLoadingState,
    TResult Function(LoginChangeFormState value)? loginChangeFormState,
    TResult Function(LoginSuccessState value)? loginSuccessState,
    TResult Function(LoginPhoneVerified value)? loginPhoneVerified,
    TResult Function(LoginPhoneNotVerified value)? loginPhoneNotVerified,
    TResult Function(ResetPinCodeState value)? resetPinCode,
    TResult Function(LoginCheckPhoneSuccessState value)?
        loginCheckPhoneSuccessState,
    TResult Function(LoginErrorState value)? loginErrorState,
    required TResult orElse(),
  }) {
    if (loginCheckPhoneSuccessState != null) {
      return loginCheckPhoneSuccessState(this);
    }
    return orElse();
  }
}

abstract class LoginCheckPhoneSuccessState extends LoginState {
  const factory LoginCheckPhoneSuccessState({final LoginViewModel viewModel}) =
      _$LoginCheckPhoneSuccessStateImpl;
  const LoginCheckPhoneSuccessState._() : super._();

  @override
  LoginViewModel get viewModel;
  @override
  @JsonKey(ignore: true)
  _$$LoginCheckPhoneSuccessStateImplCopyWith<_$LoginCheckPhoneSuccessStateImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$LoginErrorStateImplCopyWith<$Res>
    implements $LoginStateCopyWith<$Res> {
  factory _$$LoginErrorStateImplCopyWith(_$LoginErrorStateImpl value,
          $Res Function(_$LoginErrorStateImpl) then) =
      __$$LoginErrorStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({LoginViewModel viewModel, BaseException exception});

  @override
  $LoginViewModelCopyWith<$Res> get viewModel;
  $BaseExceptionCopyWith<$Res> get exception;
}

/// @nodoc
class __$$LoginErrorStateImplCopyWithImpl<$Res>
    extends _$LoginStateCopyWithImpl<$Res, _$LoginErrorStateImpl>
    implements _$$LoginErrorStateImplCopyWith<$Res> {
  __$$LoginErrorStateImplCopyWithImpl(
      _$LoginErrorStateImpl _value, $Res Function(_$LoginErrorStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? viewModel = null,
    Object? exception = null,
  }) {
    return _then(_$LoginErrorStateImpl(
      viewModel: null == viewModel
          ? _value.viewModel
          : viewModel // ignore: cast_nullable_to_non_nullable
              as LoginViewModel,
      exception: null == exception
          ? _value.exception
          : exception // ignore: cast_nullable_to_non_nullable
              as BaseException,
    ));
  }

  @override
  @pragma('vm:prefer-inline')
  $BaseExceptionCopyWith<$Res> get exception {
    return $BaseExceptionCopyWith<$Res>(_value.exception, (value) {
      return _then(_value.copyWith(exception: value));
    });
  }
}

/// @nodoc

class _$LoginErrorStateImpl extends LoginErrorState {
  const _$LoginErrorStateImpl(
      {this.viewModel = const LoginViewModel(),
      this.exception = const BaseException()})
      : super._();

  @override
  @JsonKey()
  final LoginViewModel viewModel;
  @override
  @JsonKey()
  final BaseException exception;

  @override
  String toString() {
    return 'LoginState.loginErrorState(viewModel: $viewModel, exception: $exception)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LoginErrorStateImpl &&
            (identical(other.viewModel, viewModel) ||
                other.viewModel == viewModel) &&
            (identical(other.exception, exception) ||
                other.exception == exception));
  }

  @override
  int get hashCode => Object.hash(runtimeType, viewModel, exception);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$LoginErrorStateImplCopyWith<_$LoginErrorStateImpl> get copyWith =>
      __$$LoginErrorStateImplCopyWithImpl<_$LoginErrorStateImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(LoginViewModel viewModel) loginPrimaryState,
    required TResult Function(LoginViewModel viewModel, bool shouldShowLoading)
        loginLoadingState,
    required TResult Function(LoginViewModel viewModel) loginChangeFormState,
    required TResult Function(LoginViewModel viewModel) loginSuccessState,
    required TResult Function(LoginViewModel viewModel) loginPhoneVerified,
    required TResult Function(LoginViewModel viewModel) loginPhoneNotVerified,
    required TResult Function(LoginViewModel viewModel) resetPinCode,
    required TResult Function(LoginViewModel viewModel)
        loginCheckPhoneSuccessState,
    required TResult Function(LoginViewModel viewModel, BaseException exception)
        loginErrorState,
  }) {
    return loginErrorState(viewModel, exception);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(LoginViewModel viewModel)? loginPrimaryState,
    TResult? Function(LoginViewModel viewModel, bool shouldShowLoading)?
        loginLoadingState,
    TResult? Function(LoginViewModel viewModel)? loginChangeFormState,
    TResult? Function(LoginViewModel viewModel)? loginSuccessState,
    TResult? Function(LoginViewModel viewModel)? loginPhoneVerified,
    TResult? Function(LoginViewModel viewModel)? loginPhoneNotVerified,
    TResult? Function(LoginViewModel viewModel)? resetPinCode,
    TResult? Function(LoginViewModel viewModel)? loginCheckPhoneSuccessState,
    TResult? Function(LoginViewModel viewModel, BaseException exception)?
        loginErrorState,
  }) {
    return loginErrorState?.call(viewModel, exception);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(LoginViewModel viewModel)? loginPrimaryState,
    TResult Function(LoginViewModel viewModel, bool shouldShowLoading)?
        loginLoadingState,
    TResult Function(LoginViewModel viewModel)? loginChangeFormState,
    TResult Function(LoginViewModel viewModel)? loginSuccessState,
    TResult Function(LoginViewModel viewModel)? loginPhoneVerified,
    TResult Function(LoginViewModel viewModel)? loginPhoneNotVerified,
    TResult Function(LoginViewModel viewModel)? resetPinCode,
    TResult Function(LoginViewModel viewModel)? loginCheckPhoneSuccessState,
    TResult Function(LoginViewModel viewModel, BaseException exception)?
        loginErrorState,
    required TResult orElse(),
  }) {
    if (loginErrorState != null) {
      return loginErrorState(viewModel, exception);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoginPrimaryState value) loginPrimaryState,
    required TResult Function(LoginLoadingState value) loginLoadingState,
    required TResult Function(LoginChangeFormState value) loginChangeFormState,
    required TResult Function(LoginSuccessState value) loginSuccessState,
    required TResult Function(LoginPhoneVerified value) loginPhoneVerified,
    required TResult Function(LoginPhoneNotVerified value)
        loginPhoneNotVerified,
    required TResult Function(ResetPinCodeState value) resetPinCode,
    required TResult Function(LoginCheckPhoneSuccessState value)
        loginCheckPhoneSuccessState,
    required TResult Function(LoginErrorState value) loginErrorState,
  }) {
    return loginErrorState(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(LoginPrimaryState value)? loginPrimaryState,
    TResult? Function(LoginLoadingState value)? loginLoadingState,
    TResult? Function(LoginChangeFormState value)? loginChangeFormState,
    TResult? Function(LoginSuccessState value)? loginSuccessState,
    TResult? Function(LoginPhoneVerified value)? loginPhoneVerified,
    TResult? Function(LoginPhoneNotVerified value)? loginPhoneNotVerified,
    TResult? Function(ResetPinCodeState value)? resetPinCode,
    TResult? Function(LoginCheckPhoneSuccessState value)?
        loginCheckPhoneSuccessState,
    TResult? Function(LoginErrorState value)? loginErrorState,
  }) {
    return loginErrorState?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoginPrimaryState value)? loginPrimaryState,
    TResult Function(LoginLoadingState value)? loginLoadingState,
    TResult Function(LoginChangeFormState value)? loginChangeFormState,
    TResult Function(LoginSuccessState value)? loginSuccessState,
    TResult Function(LoginPhoneVerified value)? loginPhoneVerified,
    TResult Function(LoginPhoneNotVerified value)? loginPhoneNotVerified,
    TResult Function(ResetPinCodeState value)? resetPinCode,
    TResult Function(LoginCheckPhoneSuccessState value)?
        loginCheckPhoneSuccessState,
    TResult Function(LoginErrorState value)? loginErrorState,
    required TResult orElse(),
  }) {
    if (loginErrorState != null) {
      return loginErrorState(this);
    }
    return orElse();
  }
}

abstract class LoginErrorState extends LoginState {
  const factory LoginErrorState(
      {final LoginViewModel viewModel,
      final BaseException exception}) = _$LoginErrorStateImpl;
  const LoginErrorState._() : super._();

  @override
  LoginViewModel get viewModel;
  BaseException get exception;
  @override
  @JsonKey(ignore: true)
  _$$LoginErrorStateImplCopyWith<_$LoginErrorStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
