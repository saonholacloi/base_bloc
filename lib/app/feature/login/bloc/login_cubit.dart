import 'package:base_flutter/app/definition/strings.dart';
import 'package:base_flutter/app/feature/login/bloc/login_state.dart';
import 'package:base_flutter/app/route/route.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';

@injectable
class LoginCubit extends Cubit<LoginState> {
  LoginCubit() : super(const LoginPrimaryState());

  // final LoginUseCase _loginUseCase;

  void changePhoneNumberEvent(String phoneNumber) {
    emit(LoginPrimaryState(
        viewModel: state.viewModel
            .copyWith(phoneNumber: phoneNumber, isShowPinCode: false)));
  }

  void onChangePinCode(String pinCode) {
    emit(LoginPrimaryState(
        viewModel: state.viewModel.copyWith(pinCode: pinCode)));
  }

  void goToHomePageEvent() {
    navigatorState.pushReplacementNamed(ScreenName.root);
    // navigatorState.pushReplacementNamed(ScreenName.lobby);
  }
}
