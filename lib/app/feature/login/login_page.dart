import 'package:base_flutter/app/widget/base_state.dart';
import 'package:base_flutter/design_system/ds_scaffold.dart';
import 'package:base_flutter/design_system/ds_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends BaseState<LoginPage> {
  // final LoginCubit _cubit = di();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      // _deeplinkHandler.handle();
      // widgetUtil.showGlobalLoadingOverlay();
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => DSScaffold(
        safeAreaBottom: true,
        isDisableFitTop: true,
        systemUiOverlayStyle: SystemUiOverlayStyle.dark,
        backgroundColor: DSTheme.of(context).color.mainBackgroundColor,
        statusBarBackgroundColor: Colors.transparent,
        onTap: () {
          widgetUtil.closeGlobalKeyboard();
        },
        child: const SizedBox(
            width: double.infinity,
            child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 30),
                child: SizedBox.shrink())),
      );
}
