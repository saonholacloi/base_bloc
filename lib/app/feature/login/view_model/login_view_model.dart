// ignore: depend_on_referenced_packages
import 'package:base_flutter/generated/l10n.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'login_view_model.freezed.dart';

@freezed
class LoginViewModel with _$LoginViewModel {
  const LoginViewModel._();
  const factory LoginViewModel({
    @Default('') String phoneNumber,
    @Default(false) bool isShowPinCode,
    @Default('') String pinCode,
  }) = _LoginViewModel;

  bool get isValid => isShowPinCode
      ? pinCode.isNotEmpty && phoneNumber.isNotEmpty
      : phoneNumber.isNotEmpty;
  String get phoneNumberMessage =>
      phoneNumber.isEmpty ? S.current.do_not_empty : '';
}
