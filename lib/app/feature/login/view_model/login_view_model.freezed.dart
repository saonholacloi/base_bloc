// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'login_view_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$LoginViewModel {
  String get phoneNumber => throw _privateConstructorUsedError;
  bool get isShowPinCode => throw _privateConstructorUsedError;
  String get pinCode => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $LoginViewModelCopyWith<LoginViewModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LoginViewModelCopyWith<$Res> {
  factory $LoginViewModelCopyWith(
          LoginViewModel value, $Res Function(LoginViewModel) then) =
      _$LoginViewModelCopyWithImpl<$Res, LoginViewModel>;
  @useResult
  $Res call({String phoneNumber, bool isShowPinCode, String pinCode});
}

/// @nodoc
class _$LoginViewModelCopyWithImpl<$Res, $Val extends LoginViewModel>
    implements $LoginViewModelCopyWith<$Res> {
  _$LoginViewModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? phoneNumber = null,
    Object? isShowPinCode = null,
    Object? pinCode = null,
  }) {
    return _then(_value.copyWith(
      phoneNumber: null == phoneNumber
          ? _value.phoneNumber
          : phoneNumber // ignore: cast_nullable_to_non_nullable
              as String,
      isShowPinCode: null == isShowPinCode
          ? _value.isShowPinCode
          : isShowPinCode // ignore: cast_nullable_to_non_nullable
              as bool,
      pinCode: null == pinCode
          ? _value.pinCode
          : pinCode // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$LoginViewModelImplCopyWith<$Res>
    implements $LoginViewModelCopyWith<$Res> {
  factory _$$LoginViewModelImplCopyWith(_$LoginViewModelImpl value,
          $Res Function(_$LoginViewModelImpl) then) =
      __$$LoginViewModelImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String phoneNumber, bool isShowPinCode, String pinCode});
}

/// @nodoc
class __$$LoginViewModelImplCopyWithImpl<$Res>
    extends _$LoginViewModelCopyWithImpl<$Res, _$LoginViewModelImpl>
    implements _$$LoginViewModelImplCopyWith<$Res> {
  __$$LoginViewModelImplCopyWithImpl(
      _$LoginViewModelImpl _value, $Res Function(_$LoginViewModelImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? phoneNumber = null,
    Object? isShowPinCode = null,
    Object? pinCode = null,
  }) {
    return _then(_$LoginViewModelImpl(
      phoneNumber: null == phoneNumber
          ? _value.phoneNumber
          : phoneNumber // ignore: cast_nullable_to_non_nullable
              as String,
      isShowPinCode: null == isShowPinCode
          ? _value.isShowPinCode
          : isShowPinCode // ignore: cast_nullable_to_non_nullable
              as bool,
      pinCode: null == pinCode
          ? _value.pinCode
          : pinCode // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$LoginViewModelImpl extends _LoginViewModel {
  const _$LoginViewModelImpl(
      {this.phoneNumber = '', this.isShowPinCode = false, this.pinCode = ''})
      : super._();

  @override
  @JsonKey()
  final String phoneNumber;
  @override
  @JsonKey()
  final bool isShowPinCode;
  @override
  @JsonKey()
  final String pinCode;

  @override
  String toString() {
    return 'LoginViewModel(phoneNumber: $phoneNumber, isShowPinCode: $isShowPinCode, pinCode: $pinCode)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LoginViewModelImpl &&
            (identical(other.phoneNumber, phoneNumber) ||
                other.phoneNumber == phoneNumber) &&
            (identical(other.isShowPinCode, isShowPinCode) ||
                other.isShowPinCode == isShowPinCode) &&
            (identical(other.pinCode, pinCode) || other.pinCode == pinCode));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, phoneNumber, isShowPinCode, pinCode);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$LoginViewModelImplCopyWith<_$LoginViewModelImpl> get copyWith =>
      __$$LoginViewModelImplCopyWithImpl<_$LoginViewModelImpl>(
          this, _$identity);
}

abstract class _LoginViewModel extends LoginViewModel {
  const factory _LoginViewModel(
      {final String phoneNumber,
      final bool isShowPinCode,
      final String pinCode}) = _$LoginViewModelImpl;
  const _LoginViewModel._() : super._();

  @override
  String get phoneNumber;
  @override
  bool get isShowPinCode;
  @override
  String get pinCode;
  @override
  @JsonKey(ignore: true)
  _$$LoginViewModelImplCopyWith<_$LoginViewModelImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
