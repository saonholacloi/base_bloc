import 'package:base_flutter/app/route/route.dart';
import 'package:base_flutter/design_system/ds_theme.dart';
import 'package:flutter/material.dart';

class SnackBarUtil {
  static SnackBar getSnackBar(
      BuildContext context, String message, Color color) {
    return SnackBar(
      backgroundColor: color,
      content: Text(
        message,
        style: DSTheme.of(context).style.tsQuick14w700.copyWith(
              color: Colors.white,
            ),
      ),
    );
  }

  static void showErrorSnackBar(BuildContext context, String message) {
    ScaffoldMessenger.of(context).showSnackBar(
      getSnackBar(context, message, DSTheme.of(context).color.redE72739),
    );
  }

  static void showSuccessSnackBar(BuildContext context, String message) {
    ScaffoldMessenger.of(context).showSnackBar(
      getSnackBar(context, message, DSTheme.of(context).color.successColor),
    );
  }

  static void showSuccessSnackBarGloballly(String message) {
    final BuildContext context = navigatorState.context;
    ScaffoldMessenger.of(context).showSnackBar(
      getSnackBar(context, message, DSTheme.of(context).color.successColor),
    );
  }
}
