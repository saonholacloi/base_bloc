import 'dart:math';

import 'package:injectable/injectable.dart';

@lazySingleton
class NumberUtil {
  /// Random int number, start from `zero` to [maxvalue]
  int randomInt([int maxvalue = 1000]) {
    final rng = Random();
    return rng.nextInt(maxvalue);
  }
}
