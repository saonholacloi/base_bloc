import 'package:base_flutter/app/config/network/rest_client/app_rest_client.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class AuthRepository {
  Future<void> register();
}

@Injectable(as: AuthRepository)
class AuthRepositoryImpl implements AuthRepository {
  AuthRepositoryImpl(
      this._secureStorage, this._sharedPreferences, this._restClient);

  final FlutterSecureStorage _secureStorage;
  final SharedPreferences _sharedPreferences;
  final AppRestClient _restClient;

  @override
  Future<void> register() async {
    final result = await _restClient.register();
    //return result
  }
}
