import 'package:base_flutter/app/config/exception/exception.dart';
import 'package:base_flutter/di/di.dart';
import 'package:dartz/dartz.dart';

abstract class UseCase<T, P> {
  BaseExceptionHandler? _exceptionHandler;
  BaseExceptionHandler get exceptionHandler {
    _exceptionHandler ??= di();
    return _exceptionHandler!;
  }

  Future<Either<BaseException, T>> call(P param);
}
