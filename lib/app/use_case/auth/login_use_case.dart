import 'package:base_flutter/app/config/exception/exception.dart';
import 'package:base_flutter/app/repository/auth/auth_repository.dart';
import 'package:base_flutter/app/use_case/use_case.dart';
import 'package:base_flutter/app/util/app_log_util.dart';
import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';

@injectable
class LoginUseCase extends UseCase<dynamic, LoginUseCaseParam> {
  LoginUseCase(this._repository);
  final AuthRepository _repository;
  @override
  Future<Either<BaseException, dynamic>> call(LoginUseCaseParam param) async {
    try {
      /// implement call repository
      return const Right(null);
    } catch (e, trace) {
      AppLog.error('LoginUseCase ERROR', DateTime.now(), e, trace);
      return Left(exceptionHandler.handleLoginMessage(e));
    }
  }
}

class LoginUseCaseParam {
  final String phone;
  final String pinCode;

  const LoginUseCaseParam({required this.phone, required this.pinCode});
}
