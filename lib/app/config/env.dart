enum ENVType {
  uat,
  beta,
  prod;

  bool get isDev => this == ENVType.uat;

  bool get isBeta => this == ENVType.beta;

  bool get isProd => this == ENVType.prod;

  Env toENV() {
    switch (this) {
      case uat:
        return UatEnv();
      case beta:
        return BetaEnv();
      case prod:
        return ProdEnv();
      default:
        return UatEnv();
    }
  }
}

abstract class Env {
  String get baseUrl;
}

class UatEnv implements Env {
  @override
  String get baseUrl => 'https://xyz.com/';
}

class BetaEnv implements Env {
  @override
  String get baseUrl => 'https://xyz.com/';
}

class ProdEnv implements Env {
  @override
  String baseUrl = 'https://xyz.com/';
}
