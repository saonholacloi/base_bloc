import 'package:base_flutter/app/config/env.dart';
import 'package:base_flutter/app/definition/definition.dart';
import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';
part 'app_rest_client.g.dart';

@RestApi(parser: Parser.JsonSerializable)
abstract class AppRestClient {
  factory AppRestClient.from(Dio dio, Env env) =>
      _AppRestClient(dio, baseUrl: env.baseUrl);

  @GET(EndPoint.registerClient)
  Future<dynamic> register();
}
