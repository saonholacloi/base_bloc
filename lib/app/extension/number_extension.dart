import 'package:intl/intl.dart';

extension NumberExts on num {
  static const double shortenedMilion = 1 / 1000000;
  String formatCurrency(
      {double ratio = 1, String prefix = '', String suffix = ''}) {
    final formatCurrency =
        NumberFormat.currency(locale: 'vi', decimalDigits: 0, name: '');
    return '$prefix${formatCurrency.format(this * ratio).trim()}$suffix';
  }

  String formatNumber({String format = '#,##0.0'}) {
    final formatCurrency = NumberFormat(format, 'vi_VI');
    return formatCurrency.format(this).trim();
  }

  String formatVND() => formatCurrency(suffix: 'đ');
}
